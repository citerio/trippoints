package proponomultimedia.citerio.com.trippoints.controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.model.AppDatabase;
import proponomultimedia.citerio.com.trippoints.model.Preference;
import proponomultimedia.citerio.com.trippoints.model.User;

public class Settings_Server_Screen extends AppCompatActivity {

    private Button cancel_button;
    private Button signout_button;
    private TextView email;
    private EditText name, company, address;
    private ProgressBar progressbar;
    private View decorView;
    private int uiOptions;
    private ConnectivityManager cm;
    private NetworkInfo WIFInetInfo, MOBILEnetInfo;
    private JSONObject data = new JSONObject();
    private RequestQueue queue;
    private String url = "https://heel-and-toe-galley.000webhostapp.com/Main.php";
    private static final String PROPERTY_REG_ID = "reg_id";
    private static final String TAG = "MyActivity";
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 ) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });
        setContentView(R.layout.settings_server_screen);

        cancel_button = (Button)findViewById(R.id.cancel_button);
        signout_button = (Button)findViewById(R.id.signout_button);
        email = (TextView)findViewById(R.id.email);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);

        cm = (ConnectivityManager)Settings_Server_Screen.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        queue = Volley.newRequestQueue(this);

        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        signout_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isInternetOn()){

                    try {

                        signout_button.setEnabled(false);

                        data.put("operation", "logout");
                        data.put("email", user.getEmail());
                        data.put("password", user.getPassword());

                        sendToServer(data.toString());

                    }catch (Exception e){

                        e.printStackTrace();

                    }


                }else {

                    //loginOffline();
                    noInternetConnection();

                }



            }
        });

        getUserFromDB();

    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }


    public void sendToServer(final String data){

        progressbar.setVisibility(View.VISIBLE);

        Map<String, String> params_m = new HashMap<String, String>();

        params_m.put("data", data);



        JSONObject params = new JSONObject(params_m);
        /*try {
            params.put("data", );
        } catch (JSONException e) {
            e.printStackTrace();
        }*/



        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {

                        progressbar.setVisibility(View.INVISIBLE);

                        try {

                            if(response.getString("result").equals("success")){


                                /*new AsyncTask<String, Void, String>(){


                                    private String data;
                                    private String message = "";

                                    @Override
                                    protected String doInBackground(String... params) {

                                        data = params[0];

                                        try {



                                            message = "success";
                                            return message;


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            message = "failure";
                                            return message;
                                        }


                                    }

                                    @Override
                                    protected void onPostExecute(String m) {
                                        super.onPostExecute(m);

                                        if(m.equals("success")){

                                            try {

                                                Log.v(TAG, "User info updated in DB");
                                                Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                                finish();


                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }else{

                                            Log.v(TAG, "Error on updating user info in DB");
                                            Toast.makeText(getApplicationContext(), "ERROR inside updating user", Toast.LENGTH_LONG).show();

                                        }

                                    }
                                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);*/

                                saveCurrentUser("", "", 0, false);
                                Intent logging_screen = new Intent(Settings_Server_Screen.this, Logging_Screen.class);
                                startActivity(logging_screen);
                                setResult(RESULT_OK, null);
                                finish();


                            }else if(response.getString("result").equals("failure")){

                                //Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                Snackbar
                                        .make(findViewById(R.id.parent), response.getString("message"), Snackbar.LENGTH_INDEFINITE)
                                        .setAction("Retry", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                signout_button.performClick();
                                            }
                                        })
                                        .show();
                                signout_button.setEnabled(true);

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.setVisibility(View.INVISIBLE);
                signout_button.setEnabled(true);
                Toast.makeText(getApplicationContext(), "ERROR on JsonRequest"+error.toString(), Toast.LENGTH_LONG).show();
            }

        }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        // Add the request to the RequestQueue.
        queue.add(request);

    }




    void getUserFromDB(){


        new AsyncTask<String, Void, User>(){


            @Override
            protected User doInBackground(String... params) {


                AppDatabase database = AppDatabase.getDatabase(getApplicationContext());

                user = database.userDao().getUser(getCurrentUserId());

                return user;
            }

            @Override
            protected void onPostExecute(User u) {
                super.onPostExecute(u);

                if(u != null){

                    Log.v(TAG, "USER ID: " + u.getId());
                    email.setText(u.getEmail());

                }else{

                    signout_button.setEnabled(false);
                    Toast.makeText(getApplicationContext(), "The user couldn't be loaded", Toast.LENGTH_LONG).show();

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");


    }



    public boolean isInternetOn(){

        WIFInetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        MOBILEnetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if((WIFInetInfo != null && WIFInetInfo.getState() == NetworkInfo.State.CONNECTED) || (MOBILEnetInfo != null && MOBILEnetInfo.getState() == NetworkInfo.State.CONNECTED)){

            return true;

        }else {

            return false;
        }

    }

    public void noInternetConnection(){

        Snackbar
                .make(findViewById(R.id.parent), "Geen internetverbinding", Snackbar.LENGTH_INDEFINITE)
                .setAction("Opnieuw proberen", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        signout_button.performClick();
                    }
                })
                .show();
        signout_button.setEnabled(true);

    }

    int getCurrentUserId(){


        String current_user = Preference.getString(getApplicationContext(), "current_user");


        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);

                return jsonObject_current_user.getInt("id");

            } catch (JSONException e) {


                e.printStackTrace();
            }


        }

        return 0;



    }

    String getCurrentUserEmail(){


        String current_user = Preference.getString(getApplicationContext(), "current_user");


        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);

                return jsonObject_current_user.getString("email");

            } catch (JSONException e) {


                e.printStackTrace();
            }


        }

        return "";

    }

    void saveCurrentUser(String name, String email, int id, boolean logged){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String current_user = prefs.getString("current_user", "");
        SharedPreferences.Editor editor = prefs.edit();

        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);
                jsonObject_current_user.put("name", name);
                jsonObject_current_user.put("email", email);
                jsonObject_current_user.put("id", id);
                jsonObject_current_user.put("logged", logged);

                editor.putString("current_user", jsonObject_current_user.toString());
                editor.apply();

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


    }
}
