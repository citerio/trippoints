package proponomultimedia.citerio.com.trippoints.model;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.controller.Trip_Details_Screen;

/**
 * Created by Protinal on 08/01/2018.
 */

public class TripAdapter extends RecyclerView.Adapter<TripAdapter.ViewHolder> {

    private Context nContext;
    private ArrayList<Trip> entities;


    public TripAdapter(Context context, ArrayList<Trip> entities) {

        this.nContext = context;
        this.entities = new ArrayList<Trip>();
        this.entities.addAll(entities);
    }


    @Override
    public TripAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(nContext).inflate(R.layout.trip, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bindNotification(entities.get(position));

    }

    @Override
    public int getItemCount() {
        return entities.size();
    }

    public void insert(Trip item) {
        entities.add(item);
        //notifyItemInserted(news.size()-1);

    }

    public void remove(int position) {
        entities.remove(position);
        notifyItemRemoved(position);

    }


    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public void swap(int firstPosition, int secondPosition) {
        Collections.swap(entities, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView id;
        public final ImageView map;
        public final TextView from;
        public final TextView to;
        public final TextView km;
        public final TextView date;
        public final Button trip_details_button;

        public ViewHolder(View view) {
            super(view);

            id = (TextView) view.findViewById(R.id.id);
            map = (ImageView) view.findViewById(R.id.map);
            from = (TextView) view.findViewById(R.id.from);
            to = (TextView) view.findViewById(R.id.to);
            date = (TextView) view.findViewById(R.id.date);
            km = (TextView) view.findViewById(R.id.km);
            trip_details_button = (Button) view.findViewById(R.id.trip_details_button);

            trip_details_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent trip_details_screen = new Intent(v.getContext(), Trip_Details_Screen.class);
                    trip_details_screen.putExtra("trip_id", Integer.parseInt(id.getText().toString()));


                    trip_details_screen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(trip_details_screen);


                }
            });

        }

        public void bindNotification(Trip entity) {

            if( entity.getMap() != null){
                Picasso.with(nContext).load(new File(entity.getMap())).fit().centerInside().error(R.mipmap.logo).into(this.map);
            }

            this.id.setText(Integer.toString(entity.getId()));
            this.from.setText(entity.getFrom());
            this.to.setText(entity.getTo());
            this.km.setText(Integer.toString(entity.getKm()) + " KM");

            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);

            try {
                String month_name = dateFormat.format(entity.getDate_start());
                this.date.setText(month_name);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

}
