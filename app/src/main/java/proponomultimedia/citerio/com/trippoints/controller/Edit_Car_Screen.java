package proponomultimedia.citerio.com.trippoints.controller;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.model.AppDatabase;
import proponomultimedia.citerio.com.trippoints.model.Car;
import proponomultimedia.citerio.com.trippoints.model.Preference;
import proponomultimedia.citerio.com.trippoints.model.Trip;
import proponomultimedia.citerio.com.trippoints.model.User;

public class Edit_Car_Screen extends AppCompatActivity {

    private Button cancel_button;
    private Button save_button;
    private TextInputLayout car_name_layout, car_plate_layout;
    private EditText car_name, car_plate;
    private Spinner car_manufacturer;
    private ProgressBar progressbar;
    private View decorView;
    private int uiOptions;
    private ConnectivityManager cm;
    private NetworkInfo WIFInetInfo, MOBILEnetInfo;
    private JSONObject data = new JSONObject();
    private RequestQueue queue;
    private String url = "https://heel-and-toe-galley.000webhostapp.com/Main.php";
    private static final String PROPERTY_REG_ID = "reg_id";
    private static final String TAG = "MyActivity";
    private Car car;
    private ArrayAdapter<CharSequence> car_manufacturer_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 ) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });
        setContentView(R.layout.edit_car_screen);
        cancel_button = (Button)findViewById(R.id.cancel_button);
        save_button = (Button)findViewById(R.id.save_button);
        car_name_layout = (TextInputLayout)findViewById(R.id.car_name_layout);
        car_plate_layout = (TextInputLayout)findViewById(R.id.car_plate_layout);
        car_name = (EditText)findViewById(R.id.car_name);
        car_plate = (EditText)findViewById(R.id.car_plate);
        car_manufacturer = (Spinner) findViewById(R.id.car_manufacturer);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);

        // Create an ArrayAdapter using the string array and a default spinner layout
        car_manufacturer_adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.trip_car_spinner, R.layout.brand_item);
        // Specify the layout to use when the list of choices appears
        car_manufacturer_adapter.setDropDownViewResource(R.layout.spinner_item);
        // Apply the adapter to the spinner
        car_manufacturer.setAdapter(car_manufacturer_adapter);

        car_name.addTextChangedListener(new Edit_Car_Screen.MyTextWatcher(car_name));
        car_plate.addTextChangedListener(new Edit_Car_Screen.MyTextWatcher(car_plate));

        cm = (ConnectivityManager)Edit_Car_Screen.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        queue = Volley.newRequestQueue(this);

        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isInternetOn()){

                    try {

                        if(!car_name.getText().toString().trim().isEmpty() && !car_plate.getText().toString().trim().isEmpty()){

                            save_button.setEnabled(false);

                            data.put("user_id", getCurrentUserId());
                            data.put("manufacturer", car_manufacturer.getSelectedItem().toString());
                            data.put("name", car_name.getText().toString().trim());
                            data.put("plate", car_plate.getText().toString().trim());

                            updateCarToDB(data.toString());

                        }else {

                            Toast.makeText(getApplicationContext(), getResources().getText(R.string.error_empty_field), Toast.LENGTH_LONG).show();

                        }



                    }catch (Exception e){

                        e.printStackTrace();

                    }


                }else {

                    //loginOffline();
                    noInternetConnection();

                }



            }
        });

        getCarFromDB();
    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }

    private class MyTextWatcher implements TextWatcher {


        private View view;

        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (view.getId()){
                case R.id.car_name:
                    validate_name();
                    break;
                case R.id.car_plate:
                    validate_plate();
                    break;
            }

        }
    }

    private boolean validate_name(){

        if(car_name.getText().toString().trim().isEmpty()){

            car_name_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            car_name_layout.setErrorEnabled(false);

        }

        return true;

    }

    private boolean validate_plate(){

        if(car_plate.getText().toString().trim().isEmpty()){

            car_plate_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            car_plate_layout.setErrorEnabled(false);

        }

        return true;

    }


    void addCarToDB(String data){


        new AsyncTask<String, Void, String>(){

            private String message = "";
            private String data = "";

            @Override
            protected String doInBackground(String... params) {

                try {

                    data = params[0];

                    JSONObject car_data = new JSONObject(data);

                    Car car;
                    car =  new Car(car_data.getInt("user_id"), car_data.getString("name"), car_data.getString("manufacturer"), car_data.getString("plate"));
                    AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                    database.carDao().addCar(car);
                    message = "success";
                    return message;


                } catch (Exception e) {
                    e.printStackTrace();
                    message = "failure";
                    return message;
                }

            }

            @Override
            protected void onPostExecute(String m) {
                super.onPostExecute(m);

                if(m.equals("success")){

                    try {

                        Log.v(TAG, "New car added in DB");
                        //intent_ma.putExtra("RESULT_CODE", "TRIP_REQUEST_FINISHED");
                        //getApplicationContext().sendBroadcast(intent_ma);
                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else{

                    Log.v(TAG, "Error on adding new car in DB");

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);

    }

    void updateCarToDB(String data){


        new AsyncTask<String, Void, String>(){

            private String message = "";
            private String data = "";

            @Override
            protected String doInBackground(String... params) {

                try {

                    data = params[0];

                    JSONObject car_data = new JSONObject(data);

                    car.setName(car_data.getString("name"));
                    car.setPlate(car_data.getString("plate"));
                    car.setManufacturer(car_data.getString("manufacturer"));

                    AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                    database.carDao().updateCar(car);
                    message = "success";
                    return message;


                } catch (JSONException e) {
                    e.printStackTrace();
                    message = "failure";
                    return message;
                }

            }

            @Override
            protected void onPostExecute(String m) {
                super.onPostExecute(m);

                if(m.equals("success")){

                    try {

                        Log.v(TAG, "New car updated in DB");
                        //intent_ma.putExtra("RESULT_CODE", "TRIP_REQUEST_FINISHED");
                        //getApplicationContext().sendBroadcast(intent_ma);
                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else{

                    Log.v(TAG, "Error on updating new car in DB");

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);

    }

    public boolean isInternetOn(){

        WIFInetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        MOBILEnetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if((WIFInetInfo != null && WIFInetInfo.getState() == NetworkInfo.State.CONNECTED) || (MOBILEnetInfo != null && MOBILEnetInfo.getState() == NetworkInfo.State.CONNECTED)){

            return true;

        }else {

            return false;
        }

    }

    public void noInternetConnection(){

        Snackbar
                .make(findViewById(R.id.parent), "Geen internetverbinding", Snackbar.LENGTH_INDEFINITE)
                .setAction("Opnieuw proberen", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        save_button.performClick();
                    }
                })
                .show();
        save_button.setEnabled(true);

    }

    int getCurrentUserId(){


        String current_user = Preference.getString(getApplicationContext(), "current_user");


        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);

                return jsonObject_current_user.getInt("id");

            } catch (JSONException e) {


                e.printStackTrace();
            }


        }

        return 0;

    }

    void getCarFromDB(){


        new AsyncTask<String, Void, Car>(){


            @Override
            protected Car doInBackground(String... params) {

                int car_id = getIntent().getIntExtra("car_id", 0);

                AppDatabase database = AppDatabase.getDatabase(getApplicationContext());

                car = database.carDao().getCar(getCurrentUserId(), car_id);

                return car;
            }

            @Override
            protected void onPostExecute(Car c) {
                super.onPostExecute(c);

                if(c != null){

                    car_name.setText(c.getName());
                    car_plate.setText(c.getPlate());
                    car_manufacturer.setSelection(car_manufacturer_adapter.getPosition(c.getManufacturer()));


                }else{

                    Toast.makeText(getApplicationContext(), "The car couldn't be loaded", Toast.LENGTH_LONG).show();

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");


    }
}
