package proponomultimedia.citerio.com.trippoints.model;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Created by Jose Ricardo on 10/03/2018.
 */

public class Converters {

    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
