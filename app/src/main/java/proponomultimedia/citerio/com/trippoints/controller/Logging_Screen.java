package proponomultimedia.citerio.com.trippoints.controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.firebase.iid.FirebaseInstanceId;

import proponomultimedia.citerio.com.trippoints.R;

public class Logging_Screen extends AppCompatActivity {

    private Button signin_button;
    private Button create_account_button;
    private ProgressBar progressbar;
    private View decorView;
    private int uiOptions;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 0;
    private static final String PROPERTY_REG_ID = "reg_id";
    private static final String TAG = "MyActivity";
    private String regId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 ) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });

        setContentView(R.layout.logging_screen);

        signin_button = (Button)findViewById(R.id.signin_button);
        create_account_button = (Button)findViewById(R.id.create_account_button);
        progressbar = (ProgressBar)findViewById(R.id.progressbar);

        signin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signin_screen = new Intent(Logging_Screen.this, SignIn_Screen.class);
                startActivity(signin_screen);
            }
        });

        create_account_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent create_account_screen = new Intent(Logging_Screen.this, Create_Account_Screen.class);
                startActivity(create_account_screen);
            }
        });

        ///calling FCM register process
        register();

    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }

    //////////////////////////////////////////////////GOOGLE CLOUD MESSAGING///////////////////////////////////////////////////////

    ///calling register GCM
    private void register() {
        if (checkPlayServices()) {
            try {
                regId = getRegistrationId(getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (regId.isEmpty()) {
                registerInBackground();
            } else {

                /*GcmPubSub pubSub = GcmPubSub.getInstance(this);
                try {
                    pubSub.subscribe(regId, "/topics/orbisfin" , null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getApplicationContext(), "Registration ID already exists: ", Toast.LENGTH_LONG).show();*/
                //Log.v(TAG, "valor de topic: " + pubSub.toString());
                Log.v(TAG, regId);

            }
        } else {

            Toast.makeText(getApplicationContext(), "No valid Google Play Services APK found.", Toast.LENGTH_LONG).show();

        }
    }

    ///checking if device supports googleplayservices
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    ///fetching the registration id
    private String getRegistrationId(Context context) throws Exception {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }

        return registrationId;
    }

    ///Requesting the Registration Id
    private void registerInBackground() {
        new AsyncTask<String, Void, String>() {

            String msg = "";

            @Override
            protected String doInBackground(String... params) {

                try {

                    regId = FirebaseInstanceId.getInstance().getToken();
                    msg = "success";

                    //GcmPubSub pubSub = GcmPubSub.getInstance(getApplicationContext());
                    //pubSub.subscribe(regId, "/topics/redtravel" , null);

                    //msg = "Device registered, registration ID: " + regId;

                    //sendRegistrationId(regId);
                    //msg = webserver.RegisterUser("bus1", regId);
                    Log.v(TAG, regId);
                    //Log.i(TAG, msg);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    msg = "Error :" + ex.getMessage();
                    //Log.e(TAG, msg);
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String o) {
                super.onPostExecute(o);

                //msg = msg.trim().replaceAll("\r", "");

                if(msg.equals("success")){

                    try {
                        storeRegistrationId(getApplicationContext(), regId);
                        Toast.makeText(getApplicationContext(), "successful registration", Toast.LENGTH_LONG).show();
                        Log.v(TAG, "successful registration");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }else{

                    Toast.makeText(getApplicationContext(),"error: "+msg, Toast.LENGTH_LONG).show();

                }


            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
    }


    ///Saving The registration ID
    private void storeRegistrationId(Context context, String regId) throws Exception {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.apply();

    }


}
