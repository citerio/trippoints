package proponomultimedia.citerio.com.trippoints.controller;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import proponomultimedia.citerio.com.trippoints.R;

public class Retrieve_Password_Screen extends AppCompatActivity {

    private Button retrieve_password_button;
    private ProgressBar progressbar;
    private View decorView;
    private int uiOptions;
    private TextInputLayout code_layout, password_layout;
    private EditText code, password;
    private ConnectivityManager cm;
    private NetworkInfo WIFInetInfo, MOBILEnetInfo;
    private JSONObject data = new JSONObject();
    private RequestQueue queue;
    private String url = "https://heel-and-toe-galley.000webhostapp.com/Main.php";
    private String url_main = "https://portal.shapeview.nl/";
    private String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent forgot_password_screen = getIntent();
        email = forgot_password_screen.getStringExtra("email");

        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 ) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });

        setContentView(R.layout.retrieve_password_screen);

        retrieve_password_button = (Button)findViewById(R.id.retrieve_password_button);
        code_layout = (TextInputLayout)findViewById(R.id.code_layout);
        password_layout = (TextInputLayout)findViewById(R.id.password_layout);
        code = (EditText)findViewById(R.id.code);
        password = (EditText)findViewById(R.id.password);
        progressbar = (ProgressBar)findViewById(R.id.progressbar);

        cm = (ConnectivityManager)Retrieve_Password_Screen.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        queue = Volley.newRequestQueue(this);

        code.addTextChangedListener(new Retrieve_Password_Screen.MyTextWatcher(code));
        password.addTextChangedListener(new Retrieve_Password_Screen.MyTextWatcher(password));

        retrieve_password_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(isInternetOn()){

                    try {

                        retrieve_password_button.setEnabled(false);
                        data.put("operation", "reset_pw");
                        data.put("code", code.getText().toString());
                        data.put("email", email);
                        data.put("password", password.getText().toString());

                        resetPassword(data.toString());

                    }catch (Exception e){

                        e.printStackTrace();

                    }


                }else {

                    //loginOffline();
                    noInternetConnection();

                }

                //Intent home_view = new Intent(Logging_System.this, Home_View.class);
                //startActivity(home_view);



            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }

    private class MyTextWatcher implements TextWatcher {


        private View view;

        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (view.getId()){
                case R.id.code:
                    validate_code();
                    break;
                case R.id.password:
                    validate_password();
                    break;
            }

        }
    }

    private boolean validate_code(){

        if(code.getText().toString().trim().isEmpty()){

            code_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            code_layout.setErrorEnabled(false);

        }

        return true;

    }

    private boolean validate_password(){

        if(password.getText().toString().trim().isEmpty()){

            password_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            password_layout.setErrorEnabled(false);

        }

        return true;

    }


    public void resetPassword(String data){

        progressbar.setVisibility(View.VISIBLE);

        Map<String, String> params_m = new HashMap<String, String>();

        params_m.put("data", data);

        JSONObject params = new JSONObject(params_m);

       /* JSONObject params = new JSONObject();
        try {
            params.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {

                        progressbar.setVisibility(View.INVISIBLE);

                        try {

                            if(response.getString("result").equals("success")){


                                Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                Intent logging_screen = new Intent(Retrieve_Password_Screen.this, Logging_Screen.class);
                                startActivity(logging_screen);
                                finish();


                            }else if(response.getString("result").equals("failure")){

                                //Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                Snackbar
                                        .make(findViewById(R.id.parent), response.getString("message"), Snackbar.LENGTH_INDEFINITE)
                                        .setAction("Retry", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                retrieve_password_button.performClick();
                                            }
                                        })
                                        .show();
                                retrieve_password_button.setEnabled(true);

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.setVisibility(View.INVISIBLE);
                retrieve_password_button.setEnabled(true);
                Toast.makeText(getApplicationContext(), "ERROR on JsonRequest"+error.toString(), Toast.LENGTH_LONG).show();
            }

        }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        // Add the request to the RequestQueue.
        queue.add(request);

    }

    public boolean isInternetOn(){

        WIFInetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        MOBILEnetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if((WIFInetInfo != null && WIFInetInfo.getState() == NetworkInfo.State.CONNECTED) || (MOBILEnetInfo != null && MOBILEnetInfo.getState() == NetworkInfo.State.CONNECTED)){

            return true;

        }else {

            return false;
        }

    }

    /*public void loginOffline(){


        new AsyncTask<String, Void, User>(){


            private User user;
            private String email;
            private String password;

            @Override
            protected User doInBackground(String... params) {

                email = params[0];
                password = params[1];

                AppDatabase database = AppDatabase.getDatabase(getApplicationContext());

                user = database.userDao().getUser(email, password);

                return user;
            }

            @Override
            protected void onPostExecute(User u) {
                super.onPostExecute(u);

                if(u != null){

                    saveCurrentUser(u.getEmail(), u.getId(), u.getToken(), u.getHome_title(), u.getHome_logo(), true);


                }else{

                    //Toast.makeText(getApplicationContext(), "Wrong Credentials", Toast.LENGTH_LONG).show();
                    Snackbar
                            .make(findViewById(R.id.parent), "Wrong Credentials", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    signin_button.performClick();
                                }
                            })
                            .show();
                    signin_button.setEnabled(true);

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, email.getText().toString().trim(), password.getText().toString().trim());


    }*/

    /*void saveCurrentUser(String email, int id, String token, String home_title, String home_logo, boolean logged){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String current_user = prefs.getString("current_user", "");
        SharedPreferences.Editor editor = prefs.edit();

        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);
                jsonObject_current_user.put("email", email);
                jsonObject_current_user.put("id", id);
                jsonObject_current_user.put("token", token);
                jsonObject_current_user.put("home_title", home_title);
                jsonObject_current_user.put("home_logo", home_logo);
                jsonObject_current_user.put("logged", logged);

                editor.putString("current_user", jsonObject_current_user.toString());
                editor.apply();

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


    }*/

    public void noInternetConnection(){

        Snackbar
                .make(findViewById(R.id.parent), "Geen internetverbinding", Snackbar.LENGTH_INDEFINITE)
                .setAction("Opnieuw proberen", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        retrieve_password_button.performClick();
                    }
                })
                .show();
        retrieve_password_button.setEnabled(true);

    }

}
