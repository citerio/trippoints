package proponomultimedia.citerio.com.trippoints.model;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.controller.Edit_Car_Screen;
import proponomultimedia.citerio.com.trippoints.controller.Trip_Details_Screen;

/**
 * Created by Protinal on 08/01/2018.
 */

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {

    private Context nContext;
    private ArrayList<Car> entities;


    public CarAdapter(Context context, ArrayList<Car> entities) {

        this.nContext = context;
        this.entities = new ArrayList<Car>();
        this.entities.addAll(entities);
    }


    @Override
    public CarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(nContext).inflate(R.layout.car, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bindNotification(entities.get(position));

    }

    @Override
    public int getItemCount() {
        return entities.size();
    }

    public void insert(Car item) {
        entities.add(item);
        //notifyItemInserted(news.size()-1);

    }

    public void remove(int position) {
        entities.remove(position);
        notifyItemRemoved(position);

    }


    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public void swap(int firstPosition, int secondPosition) {
        Collections.swap(entities, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView id;
        public final TextView name;


        public ViewHolder(View view) {
            super(view);

            id = (TextView) view.findViewById(R.id.id);
            name = (TextView) view.findViewById(R.id.name);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent edit_car_screen = new Intent(v.getContext(), Edit_Car_Screen.class);
                    edit_car_screen.putExtra("car_id", Integer.parseInt(id.getText().toString()));
                    edit_car_screen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(edit_car_screen);


                }
            });

        }

        public void bindNotification(Car entity) {

            this.id.setText(Integer.toString(entity.getId()));
            this.name.setText(entity.getName());


        }


    }

}
