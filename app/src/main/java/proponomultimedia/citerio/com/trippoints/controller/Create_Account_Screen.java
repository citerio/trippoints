package proponomultimedia.citerio.com.trippoints.controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.model.AppDatabase;
import proponomultimedia.citerio.com.trippoints.model.User;

public class Create_Account_Screen extends AppCompatActivity {

    private Button create_account_button;
    private ProgressBar progressbar;
    private View decorView;
    private int uiOptions;
    private TextInputLayout email_layout, password_layout, name_layout, company_layout, address_layout, phone_layout;
    private EditText email, password, name, company, address, phone;
    private ConnectivityManager cm;
    private NetworkInfo WIFInetInfo, MOBILEnetInfo;
    private JSONObject data = new JSONObject();
    private RequestQueue queue;
    private String url = "https://heel-and-toe-galley.000webhostapp.com/Main.php";
    private String url_main = "https://portal.shapeview.nl/";
    private TextView forgot_password_buton;
    private static final String PROPERTY_REG_ID = "reg_id";
    private String regId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 ) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });

        setContentView(R.layout.create_account_screen);

        create_account_button = (Button)findViewById(R.id.create_account_button);
        email_layout = (TextInputLayout)findViewById(R.id.email_layout);
        password_layout = (TextInputLayout)findViewById(R.id.password_layout);
        name_layout = (TextInputLayout)findViewById(R.id.name_layout);
        company_layout = (TextInputLayout)findViewById(R.id.company_layout);
        address_layout = (TextInputLayout)findViewById(R.id.address_layout);
        phone_layout = (TextInputLayout)findViewById(R.id.phone_layout);
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);
        name = (EditText)findViewById(R.id.name);
        company = (EditText)findViewById(R.id.company);
        address = (EditText)findViewById(R.id.address);
        phone = (EditText)findViewById(R.id.phone);
        forgot_password_buton = (TextView)findViewById(R.id.forgot_password_button);
        progressbar = (ProgressBar)findViewById(R.id.progressbar);

        cm = (ConnectivityManager)Create_Account_Screen.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        queue = Volley.newRequestQueue(this);

        email.addTextChangedListener(new Create_Account_Screen.MyTextWatcher(email));
        password.addTextChangedListener(new Create_Account_Screen.MyTextWatcher(password));
        name.addTextChangedListener(new Create_Account_Screen.MyTextWatcher(name));
        company.addTextChangedListener(new Create_Account_Screen.MyTextWatcher(company));
        address.addTextChangedListener(new Create_Account_Screen.MyTextWatcher(address));
        phone.addTextChangedListener(new Create_Account_Screen.MyTextWatcher(phone));

        create_account_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(isInternetOn()){

                    try {

                        create_account_button.setEnabled(false);

                        regId = getRegistrationId(getApplicationContext());

                        data.put("operation", "create");
                        data.put("email", email.getText().toString().trim());
                        data.put("password", password.getText().toString().trim());
                        data.put("name", name.getText().toString().trim());
                        data.put("company", company.getText().toString().trim());
                        data.put("address", address.getText().toString().trim());
                        data.put("phone_number", phone.getText().toString().trim());
                        data.put("token", regId);

                        createAccount(data.toString());

                    }catch (Exception e){

                        e.printStackTrace();

                    }


                }else {

                    //loginOffline();
                    noInternetConnection();

                }

                //Intent home_view = new Intent(Logging_System.this, Home_View.class);
                //startActivity(home_view);



            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }

    private class MyTextWatcher implements TextWatcher {


        private View view;

        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (view.getId()){
                case R.id.email:
                    validate_email();
                    break;
                case R.id.password:
                    validate_password();
                    break;
                case R.id.name:
                    validate_name();
                    break;
                case R.id.company:
                    validate_company();
                    break;
                case R.id.address:
                    validate_address();
                    break;
                case R.id.phone:
                    validate_phone();
                    break;
            }

        }
    }

    private boolean validate_email(){

        if(email.getText().toString().trim().isEmpty()){

            email_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            email_layout.setErrorEnabled(false);

        }

        return true;

    }

    private boolean validate_password(){

        if(password.getText().toString().trim().isEmpty()){

            password_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            password_layout.setErrorEnabled(false);

        }

        return true;

    }


    private boolean validate_name(){

        if(name.getText().toString().trim().isEmpty()){

            name_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            name_layout.setErrorEnabled(false);

        }

        return true;

    }

    private boolean validate_company(){

        if(company.getText().toString().trim().isEmpty()){

            company_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            company_layout.setErrorEnabled(false);

        }

        return true;

    }

    private boolean validate_address(){

        if(address.getText().toString().trim().isEmpty()){

            address_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            address_layout.setErrorEnabled(false);

        }

        return true;

    }


    private boolean validate_phone(){

        if(phone.getText().toString().trim().isEmpty()){

            phone_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            phone_layout.setErrorEnabled(false);

        }

        return true;

    }

    public void createAccount(String data){

        progressbar.setVisibility(View.VISIBLE);

        Map<String, String> params_m = new HashMap<String, String>();

        params_m.put("data", data);

        JSONObject params = new JSONObject(params_m);

       /* JSONObject params = new JSONObject();
        try {
            params.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {

                        progressbar.setVisibility(View.INVISIBLE);

                        try {

                            if(response.getString("result").equals("success")){


                                Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                Intent confirm_email_screen = new Intent(Create_Account_Screen.this, Confirm_Email_Screen.class);
                                confirm_email_screen.putExtra("email", email.getText().toString().trim());
                                confirm_email_screen.putExtra("password", password.getText().toString().trim());
                                startActivity(confirm_email_screen);
                                finish();


                            }else if(response.getString("result").equals("failure")){

                                //Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                Snackbar
                                        .make(findViewById(R.id.parent), response.getString("message"), Snackbar.LENGTH_INDEFINITE)
                                        .setAction("Retry", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                create_account_button.performClick();
                                            }
                                        })
                                        .show();
                                create_account_button.setEnabled(true);

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.setVisibility(View.INVISIBLE);
                create_account_button.setEnabled(true);
                Toast.makeText(getApplicationContext(), "ERROR on JsonRequest"+error.toString(), Toast.LENGTH_LONG).show();
            }

        }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                //headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        // Add the request to the RequestQueue.
        queue.add(request);

    }

    public boolean isInternetOn(){

        WIFInetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        MOBILEnetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if((WIFInetInfo != null && WIFInetInfo.getState() == NetworkInfo.State.CONNECTED) || (MOBILEnetInfo != null && MOBILEnetInfo.getState() == NetworkInfo.State.CONNECTED)){

            return true;

        }else {

            return false;
        }

    }

    /*public void loginOffline(){


        new AsyncTask<String, Void, User>(){


            private User user;
            private String email;
            private String password;

            @Override
            protected User doInBackground(String... params) {

                email = params[0];
                password = params[1];

                AppDatabase database = AppDatabase.getDatabase(getApplicationContext());

                user = database.userDao().getUser(email, password);

                return user;
            }

            @Override
            protected void onPostExecute(User u) {
                super.onPostExecute(u);

                if(u != null){

                    saveCurrentUser(u.getEmail(), u.getId(), u.getToken(), u.getHome_title(), u.getHome_logo(), true);


                }else{

                    //Toast.makeText(getApplicationContext(), "Wrong Credentials", Toast.LENGTH_LONG).show();
                    Snackbar
                            .make(findViewById(R.id.parent), "Wrong Credentials", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    signin_button.performClick();
                                }
                            })
                            .show();
                    signin_button.setEnabled(true);

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, email.getText().toString().trim(), password.getText().toString().trim());


    }*/

    void saveCurrentUser(String email, int id, boolean logged){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String current_user = prefs.getString("current_user", "");
        SharedPreferences.Editor editor = prefs.edit();

        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);
                jsonObject_current_user.put("email", email);
                jsonObject_current_user.put("id", id);
                jsonObject_current_user.put("logged", logged);

                editor.putString("current_user", jsonObject_current_user.toString());
                editor.apply();

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


    }

    public void noInternetConnection(){

        Snackbar
                .make(findViewById(R.id.parent), "Geen internetverbinding", Snackbar.LENGTH_INDEFINITE)
                .setAction("Opnieuw proberen", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        create_account_button.performClick();
                    }
                })
                .show();
        create_account_button.setEnabled(true);

    }

    ///fetching the registration id
    private String getRegistrationId(Context context) throws Exception {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }

        return registrationId;
    }

}
