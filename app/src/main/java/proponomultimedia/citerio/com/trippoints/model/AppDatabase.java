package proponomultimedia.citerio.com.trippoints.model;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

/**
 * Created by Administrador on 13/09/2017.
 */
@Database(entities = {User.class, Trip.class, Car.class, LocationObject.class}, version = 2)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase{

    private static AppDatabase INSTANCE;

    public abstract UserDao userDao();
    public abstract TripDao tripDao();
    public abstract CarDao carDao();
    public abstract LocationObjectDao locationObjectDao();

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context, AppDatabase.class, "trip_points")
                    //Room.inMemoryDatabaseBuilder(context.getApplicationContext(), AppDatabase.class)
                            //.addMigrations(MIGRATION_1_2)
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //database.execSQL("ALTER TABLE Activity" + " ADD COLUMN type INTEGER NOT NULL DEFAULT 1");
            database.execSQL("ALTER TABLE Trip" + " ADD COLUMN map TEXT");
            //database.execSQL("ALTER TABLE Activity" + " ADD COLUMN text_stamp TEXT");
        }
    };

}
