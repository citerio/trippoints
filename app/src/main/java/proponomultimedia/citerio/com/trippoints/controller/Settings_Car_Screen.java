package proponomultimedia.citerio.com.trippoints.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.model.AppDatabase;
import proponomultimedia.citerio.com.trippoints.model.Car;
import proponomultimedia.citerio.com.trippoints.model.CarAdapter;
import proponomultimedia.citerio.com.trippoints.model.Preference;
import proponomultimedia.citerio.com.trippoints.model.Trip;
import proponomultimedia.citerio.com.trippoints.model.TripAdapter;

public class Settings_Car_Screen extends AppCompatActivity {

    private RecyclerView car_list;
    private RecyclerView.LayoutManager car_LayoutManager;
    private TextView no_cars_recorded;
    private Button add_button;
    private ProgressBar progressbar;
    private View decorView;
    private CardView car_list_card;
    private int uiOptions;
    private CarBroadCastReceiver carReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 ) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });
        setContentView(R.layout.settings_car_screen);

        carReceiver = new CarBroadCastReceiver();

        car_list = (RecyclerView) findViewById(R.id.car_list);
        no_cars_recorded = (TextView) findViewById(R.id.no_cars_recorded);
        add_button = (Button) findViewById(R.id.add_button);
        car_list_card = (CardView) findViewById(R.id.car_list_card);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);

        car_LayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        car_list.setLayoutManager(car_LayoutManager);
        car_list.setItemAnimator(new DefaultItemAnimator());
        car_list.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));

        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent new_car_screen = new Intent(Settings_Car_Screen.this, New_Car_Screen.class);
                startActivity(new_car_screen);
            }
        });

        showSectionBody();
    }

    public void showSectionBody(){


        new AsyncTask<String, Void, CarAdapter>() {


            private CarAdapter car_adapter = null;

            @Override
            protected CarAdapter doInBackground(String... params) {


                /////////////////////////////////Video section/////////////////////////////////
                AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                car_adapter = new CarAdapter(getApplicationContext(), (ArrayList<Car>) database.carDao().getCars(getCurrentUserId()));
                return car_adapter;
            }

            @Override
            protected void onPostExecute(CarAdapter s) {
                super.onPostExecute(s);

                if(car_adapter != null){

                    car_list.setAdapter(car_adapter);

                    if(car_list.getAdapter().getItemCount() == 0){

                        no_cars_recorded.setVisibility(View.VISIBLE);
                        car_list_card.setVisibility(View.GONE);

                    }else if(car_list.getAdapter().getItemCount() > 0){

                        no_cars_recorded.setVisibility(View.GONE);
                        car_list_card.setVisibility(View.VISIBLE);
                    }


                }else{

                    Toast.makeText(getApplicationContext(), "car adapter null",  Toast.LENGTH_LONG).show();


                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");



    }

    int getCurrentUserId(){


        String current_user = Preference.getString(getApplicationContext(), "current_user");


        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);

                return jsonObject_current_user.getInt("id");

            } catch (JSONException e) {


                e.printStackTrace();
            }


        }

        return 0;

    }

    private class CarBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String local = intent.getExtras().getString("RESULT_CODE");
            assert local != null;
            if(local.equals("NEW_CAR_ADDED")){
                //get all data from database
                showSectionBody();

            }
        }
    }

    @Override
    public void onStart() {
        getApplicationContext().registerReceiver(carReceiver, new IntentFilter("proponomultimedia.citerio.com.trippoints"));
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getApplicationContext().unregisterReceiver(carReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }
}
