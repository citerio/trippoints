package proponomultimedia.citerio.com.trippoints.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.Date;
import java.util.List;

/**
 * Created by Jose Ricardo on 13/09/2017.
 */
@Dao
public interface CarDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addCar(Car car);

    @Query("SELECT * FROM car WHERE user_id = :id ORDER BY name ASC")
    List<Car> getCars(int id);

    @Query("SELECT * FROM car WHERE user_id = :user_id AND id = :id")
    Car getCar(int user_id, int id);

    @Query("DELETE FROM car")
    void removeAllCars();

    @Delete
    void removeCar(Car car);

    @Update
    void updateCar(Car car);

}
