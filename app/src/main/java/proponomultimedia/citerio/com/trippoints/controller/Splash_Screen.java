package proponomultimedia.citerio.com.trippoints.controller;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

public class Splash_Screen extends AppCompatActivity {

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);


        new Handler().postDelayed(new Runnable(){
            @Override
            public void run(){

                if(logged()){

                    Intent employee_tabs_screen = new Intent(Splash_Screen.this, Employee_Tabs_Screen.class);
                    startActivity(employee_tabs_screen);
                    finish();

                }else {

                    Intent logging_screen = new Intent(Splash_Screen.this, Logging_Screen.class);
                    startActivity(logging_screen);
                    finish();

                }

            }
        },3000);
    }


    boolean logged(){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String current_user = prefs.getString("current_user", "");

        SharedPreferences.Editor editor = prefs.edit();

        if(current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject();
                jsonObject_current_user.put("logged", false);

                editor.putString("current_user", jsonObject_current_user.toString());
                editor.apply();

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return false;


        }else {

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);

                return jsonObject_current_user.getBoolean("logged");

            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }


        }

    }
}
