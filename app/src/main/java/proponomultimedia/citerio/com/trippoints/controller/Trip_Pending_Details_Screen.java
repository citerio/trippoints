package proponomultimedia.citerio.com.trippoints.controller;

import android.content.Intent;
import android.os.AsyncTask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


import java.text.SimpleDateFormat;
import java.util.Locale;

import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.model.AppDatabase;
import proponomultimedia.citerio.com.trippoints.model.Car;
import proponomultimedia.citerio.com.trippoints.model.Preference;
import proponomultimedia.citerio.com.trippoints.model.Trip;

public class Trip_Pending_Details_Screen extends AppCompatActivity {

    private Button cancel_button;
    private Button ok_button;
    private TextView trip_total_distance, trip_total_amount;
    private TextView trip_purpose, trip_from, trip_to, trip_date, trip_car, trip_time, trip_odometer_start, trip_odometer_end, trip_notes;
    private ProgressBar progressbar;
    private View decorView;
    private int uiOptions;
    private final int TRIP_PENDING = 1;
    private final int TRIP_APPROVED = 2;
    private final int TRIP_ONGOING = 3;
    private final int TRIP_FINISHED = 4;
    public static final String ACTION = "proponomultimedia.citerio.com.trippoints";
    private Intent intent_ma = new Intent(ACTION);
    private Trip trip;
    private static final String TAG = "MyActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 ) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });
        setContentView(R.layout.trip_pending_details_screen);

        cancel_button = (Button)findViewById(R.id.cancel_button);
        ok_button = (Button)findViewById(R.id.ok_button);
        trip_purpose = (TextView) findViewById(R.id.trip_purpose);
        trip_from = (TextView) findViewById(R.id.trip_from);
        trip_to = (TextView) findViewById(R.id.trip_to);
        trip_date = (TextView) findViewById(R.id.trip_date);
        trip_car = (TextView) findViewById(R.id.trip_car);
        //trip_time = (EditText) findViewById(R.id.trip_time);

        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteTripFromDB();
            }
        });

        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        getPendingTripFromDB();

    }

    void getPendingTripFromDB(){


        new AsyncTask<String, Void, Trip>(){

            private Car car;

            @Override
            protected Trip doInBackground(String... params) {

                try{

                    AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                    trip = database.tripDao().getTrip(getCurrentUserId(), TRIP_PENDING);

                    if(trip != null){

                        car = database.carDao().getCar(getCurrentUserId(), trip.getCar());

                    }

                    return trip;

                }catch (Exception e){

                    e.printStackTrace();
                    return null;

                }

            }

            @Override
            protected void onPostExecute(Trip t) {
                super.onPostExecute(t);

                if(t != null){

                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);

                    trip_date.setText(dateFormat.format(t.getDate_start()));

                    //trip_time.setText(t.getTime_start());
                    trip_purpose.setText(t.getPurpose());
                    trip_from.setText(t.getFrom());
                    trip_to.setText(t.getTo());

                    if (car != null){

                        trip_car.setText(car.getName() + " " + car.getManufacturer());

                    }

                }else{

                    Toast.makeText(getApplicationContext(), "The pending trip couldn't be loaded", Toast.LENGTH_LONG).show();

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");


    }

    void deleteTripFromDB(){


        new AsyncTask<String, Void, String>(){

            private String message = "";

            @Override
            protected String doInBackground(String... params) {

                try {

                    AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                    database.tripDao().removeTrip(trip);
                    message = "success";
                    return message;


                } catch (Exception e) {
                    e.printStackTrace();
                    message = "failure";
                    return message;
                }

            }

            @Override
            protected void onPostExecute(String m) {
                super.onPostExecute(m);

                if(m.equals("success")){

                    try {

                        Log.v(TAG, "New trip removed in DB");
                        intent_ma.putExtra("RESULT_CODE", "TRIP_REQUEST_FINISHED");
                        getApplicationContext().sendBroadcast(intent_ma);
                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else{

                    Log.v(TAG, "Error on removing new trip in DB");

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");

    }

    int getCurrentUserId(){


        String current_user = Preference.getString(getApplicationContext(), "current_user");


        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);

                return jsonObject_current_user.getInt("id");

            } catch (JSONException e) {


                e.printStackTrace();
            }


        }

        return 0;



    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }
}
