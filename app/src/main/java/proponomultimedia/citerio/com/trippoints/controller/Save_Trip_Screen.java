package proponomultimedia.citerio.com.trippoints.controller;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.model.AppDatabase;
import proponomultimedia.citerio.com.trippoints.model.Preference;
import proponomultimedia.citerio.com.trippoints.model.Trip;

public class Save_Trip_Screen extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener{

    private Button cancel_button;
    private Button save_button;
    private ImageButton trip_from_button, trip_to_button;
    private TextView trip_total_distance, trip_total_amount, trip_date, trip_time;
    private EditText trip_purpose, trip_from, trip_to, trip_odometer_start, trip_odometer_end, trip_notes;
    private TextInputLayout trip_purpose_layout, trip_from_layout, trip_to_layout, trip_date_layout, trip_time_layout, trip_odometer_start_layout, trip_odometer_end_layout, trip_notes_layout;
    private ProgressBar progressbar;
    private View decorView;
    private int uiOptions;
    private int TRIP_FROM = 1;
    private int TRIP_TO = 2;
    private static final String TAG = "MyActivity";
    private JSONObject data = new JSONObject();
    private ConnectivityManager cm;
    private NetworkInfo WIFInetInfo, MOBILEnetInfo;
    public static final String ACTION = "proponomultimedia.citerio.com.trippoints";
    private Intent intent_ma = new Intent(ACTION);
    private int user_id = 0;
    private final int TRIP_PENDING = 1;
    private final int TRIP_APPROVED = 2;
    private final int TRIP_ONGOING = 3;
    private final int TRIP_FINISHED = 4;
    private Trip trip;
    private Spinner trip_type;
    private final int TRIP_TYPE_BUSINESS = 0;
    private final int TRIP_TYPE_PRIVATE = 1;
    private String url = "https://heel-and-toe-galley.000webhostapp.com/Main.php";
    private RequestQueue queue;
    private SimpleDateFormat dateFormatTextView;
    private SimpleDateFormat dateFormatDataBase;
    private SimpleDateFormat timeFormatDataBase;
    private SimpleDateFormat timeFormatTextView;
    private Date start_date_search, start_time_search;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 ) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });
        setContentView(R.layout.save_trip_screen);

        cancel_button = (Button)findViewById(R.id.cancel_button);
        save_button = (Button)findViewById(R.id.save_button);
        trip_from_button = (ImageButton) findViewById(R.id.trip_from_button);
        trip_to_button = (ImageButton) findViewById(R.id.trip_to_button);
        trip_purpose = (EditText) findViewById(R.id.trip_purpose);
        trip_from = (EditText) findViewById(R.id.trip_from);
        trip_to = (EditText) findViewById(R.id.trip_to);
        trip_date = (TextView) findViewById(R.id.trip_date);
        trip_time = (TextView) findViewById(R.id.trip_time);
        trip_notes = (EditText) findViewById(R.id.trip_notes);
        trip_odometer_start = (EditText) findViewById(R.id.trip_odometer_start);
        trip_odometer_end = (EditText) findViewById(R.id.trip_odometer_end);
        trip_type = (Spinner) findViewById(R.id.trip_type);
        trip_total_distance = (TextView) findViewById(R.id.trip_total_distance);
        trip_total_amount = (TextView) findViewById(R.id.trip_total_amount);
        trip_purpose_layout = (TextInputLayout)findViewById(R.id.trip_purpose_layout);
        trip_from_layout = (TextInputLayout)findViewById(R.id.trip_from_layout);
        trip_to_layout = (TextInputLayout)findViewById(R.id.trip_to_layout);
        trip_date_layout = (TextInputLayout)findViewById(R.id.trip_date_layout);
        trip_time_layout = (TextInputLayout)findViewById(R.id.trip_time_layout);
        trip_odometer_start_layout= (TextInputLayout)findViewById(R.id.trip_odometer_start_layout);
        trip_odometer_end_layout= (TextInputLayout)findViewById(R.id.trip_odometer_end_layout);
        trip_notes_layout = (TextInputLayout)findViewById(R.id.trip_notes_layout);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> trip_type_adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.type_trip_export, R.layout.brand_item);
        // Specify the layout to use when the list of choices appears
        trip_type_adapter.setDropDownViewResource(R.layout.spinner_item);
        // Apply the adapter to the spinner
        trip_type.setAdapter(trip_type_adapter);

        dateFormatTextView = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);
        dateFormatDataBase = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        timeFormatDataBase = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        timeFormatTextView = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);

        cm = (ConnectivityManager)Save_Trip_Screen.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        queue = Volley.newRequestQueue(this);

        trip_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(v);
            }
        });

        trip_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(v);
            }
        });

        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteTripFromDB();
            }
        });

        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isInternetOn()){

                    try {

                        save_button.setEnabled(false);

                        user_id = getCurrentUserId();

                        data.put("operation", "trip_update");
                        data.put("id", trip.getId());
                        data.put("date_start", dateFormatDataBase.format(start_date_search));
                        data.put("date_end", dateFormatDataBase.format(new Date()));
                        data.put("time_start", timeFormatDataBase.format(start_time_search));
                        data.put("time_end", timeFormatDataBase.format(new Date()));
                        data.put("purpose", trip_purpose.getText().toString().trim());
                        data.put("from", trip_from.getText().toString().trim());
                        data.put("to", trip_to.getText().toString().trim());
                        data.put("odometer_start", trip_odometer_start.getText().toString().trim());
                        data.put("odometer_end", trip_odometer_end.getText().toString().trim());

                        String trip_type_string = (String) trip_type.getSelectedItem();

                        if(trip_type_string.equals("Poslovanje")){

                            data.put("type", TRIP_TYPE_BUSINESS);


                        }else if(trip_type_string.equals("Privatna")){

                            data.put("type", TRIP_TYPE_PRIVATE);

                        }

                        int trip_distance = Integer.parseInt(trip_odometer_end.getText().toString().trim()) - Integer.parseInt(trip_odometer_start.getText().toString().trim());

                        data.put("distance", trip_distance);
                        data.put("amount", 0);
                        data.put("notes", trip_notes.getText().toString().trim());
                        data.put("user", user_id);
                        data.put("status", TRIP_FINISHED);
                        data.put("car", "");


                        sendToServer(data.toString());

                    }catch (Exception e){

                        e.printStackTrace();

                    }


                }else {

                    //loginOffline();
                    noInternetConnection();

                }



            }
        });

        trip_from_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findPlaceFrom();
            }
        });

        trip_to_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findPlaceTo();
            }
        });

        trip_from.addTextChangedListener(new Save_Trip_Screen.MyTextWatcher(trip_from));
        trip_to.addTextChangedListener(new Save_Trip_Screen.MyTextWatcher(trip_to));
        trip_purpose.addTextChangedListener(new Save_Trip_Screen.MyTextWatcher(trip_purpose));

        getPendingTripFromDB();


    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }

    private class MyTextWatcher implements TextWatcher {


        private View view;

        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (view.getId()){
                case R.id.trip_from:
                    validate_from();
                    break;
                case R.id.trip_to:
                    validate_to();
                    break;
                case R.id.trip_purpose:
                    validate_purpose();
                    break;
            }

        }
    }

    private boolean validate_purpose(){

        if(trip_purpose.getText().toString().trim().isEmpty()){

            trip_purpose_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            trip_purpose_layout.setErrorEnabled(false);

        }

        return true;

    }

    private boolean validate_to(){

        if(trip_to.getText().toString().trim().isEmpty()){

            trip_to_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            trip_to_layout.setErrorEnabled(false);

        }

        return true;

    }

    private boolean validate_from(){

        if(trip_from.getText().toString().trim().isEmpty()){

            trip_from_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            trip_from_layout.setErrorEnabled(false);

        }

        return true;

    }

    public void findPlaceFrom() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            //.setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, TRIP_FROM);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void findPlaceTo() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            //.setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, TRIP_TO);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    // A place has been received; use requestCode to track the request.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TRIP_FROM) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                trip_from.setText(place.getName());
                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        if (requestCode == TRIP_TO) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                trip_to.setText(place.getName());
                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    public void sendToServer(final String data){

        progressbar.setVisibility(View.VISIBLE);

        Map<String, String> params_m = new HashMap<String, String>();

        params_m.put("data", data);



        JSONObject params = new JSONObject(params_m);
        /*try {
            params.put("data", );
        } catch (JSONException e) {
            e.printStackTrace();
        }*/



        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {

                        progressbar.setVisibility(View.INVISIBLE);

                        try {

                            if(response.getString("result").equals("success")){


                                new AsyncTask<String, Void, String>(){


                                    private String data;
                                    private String message = "";

                                    @Override
                                    protected String doInBackground(String... params) {

                                        data = params[0];

                                        try {

                                            data = params[0];

                                            JSONObject trip_data = new JSONObject(data);

                                            trip.setDate_start(dateFormatDataBase.parse(trip_data.getString("date_start")));
                                            trip.setDate_end(dateFormatDataBase.parse(trip_data.getString("date_end")));
                                            trip.setTime_start(trip_data.getString("time_start"));
                                            trip.setTime_end(trip_data.getString("time_end"));
                                            trip.setPurpose(trip_data.getString("purpose"));
                                            trip.setFrom(trip_data.getString("from"));
                                            trip.setTo(trip_data.getString("to"));
                                            trip.setOdometer_start(trip_data.getInt("odometer_start"));
                                            trip.setOdometer_end(trip_data.getInt("odometer_end"));
                                            trip.setType(trip_data.getInt("type"));
                                            trip.setKm(trip_data.getInt("distance"));
                                            trip.setAmount(trip_data.getInt("amount"));
                                            trip.setNotes(trip_data.getString("notes"));
                                            trip.setUser(trip_data.getInt("user"));
                                            trip.setStatus(trip_data.getInt("status"));
                                            trip.setMap(getIntent().getStringExtra("map"));

                                            AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                                            database.tripDao().updateTrip(trip);

                                            message = "success";
                                            return message;


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            message = "failure";
                                            return message;
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            message = "failure";
                                            return message;
                                        }


                                    }

                                    @Override
                                    protected void onPostExecute(String m) {
                                        super.onPostExecute(m);

                                        if(m.equals("success")){

                                            try {

                                                Log.v(TAG, "New trip updated in DB");
                                                intent_ma.putExtra("RESULT_CODE", "TRIP_REQUEST_FINISHED");
                                                getApplicationContext().sendBroadcast(intent_ma);
                                                Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                                finish();


                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }else{

                                            Log.v(TAG, "Error on updating new trip in DB");
                                            Toast.makeText(getApplicationContext(), "ERROR inside updating trip", Toast.LENGTH_LONG).show();

                                        }

                                    }
                                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);


                            }else if(response.getString("result").equals("failure")){

                                //Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                Snackbar
                                        .make(findViewById(R.id.parent), response.getString("message"), Snackbar.LENGTH_INDEFINITE)
                                        .setAction("Retry", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                save_button.performClick();
                                            }
                                        })
                                        .show();
                                save_button.setEnabled(true);

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.setVisibility(View.INVISIBLE);
                save_button.setEnabled(true);
                Toast.makeText(getApplicationContext(), "ERROR on JsonRequest"+error.toString(), Toast.LENGTH_LONG).show();
            }

        }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        // Add the request to the RequestQueue.
        queue.add(request);

    }


    void updateTripToDB(String data){


        new AsyncTask<String, Void, String>(){

            private String message = "";
            private String data = "";

            @Override
            protected String doInBackground(String... params) {

                try {

                    data = params[0];

                    JSONObject trip_data = new JSONObject(data);

                    trip.setDate_start(dateFormatDataBase.parse(trip_data.getString("date_start")));
                    trip.setDate_end(dateFormatDataBase.parse(trip_data.getString("date_end")));
                    trip.setTime_start(trip_data.getString("time_start"));
                    trip.setTime_end(trip_data.getString("time_end"));
                    trip.setPurpose(trip_data.getString("purpose"));
                    trip.setFrom(trip_data.getString("from"));
                    trip.setTo(trip_data.getString("to"));
                    trip.setOdometer_start(trip_data.getInt("odometer_start"));
                    trip.setOdometer_end(trip_data.getInt("odometer_end"));
                    trip.setType(trip_data.getInt("type"));
                    trip.setKm(trip_data.getInt("km"));
                    trip.setAmount(trip_data.getInt("amount"));
                    trip.setNotes(trip_data.getString("notes"));
                    trip.setUser(trip_data.getInt("user"));
                    trip.setStatus(trip_data.getInt("status"));

                    AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                    database.tripDao().updateTrip(trip);
                    message = "success";
                    return message;


                } catch (Exception e) {
                    e.printStackTrace();
                    message = "failure";
                    return message;
                }

            }

            @Override
            protected void onPostExecute(String m) {
                super.onPostExecute(m);

                if(m.equals("success")){

                    try {

                        Log.v(TAG, "New trip updated in DB");
                        intent_ma.putExtra("RESULT_CODE", "TRIP_REQUEST_FINISHED");
                        getApplicationContext().sendBroadcast(intent_ma);
                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else{

                    Log.v(TAG, "Error on updating new trip in DB");

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);

    }

    void getPendingTripFromDB(){


        new AsyncTask<String, Void, Trip>(){


            @Override
            protected Trip doInBackground(String... params) {


                AppDatabase database = AppDatabase.getDatabase(getApplicationContext());

                trip = database.tripDao().getTrip(getCurrentUserId(), TRIP_ONGOING);

                return trip;
            }

            @Override
            protected void onPostExecute(Trip t) {
                super.onPostExecute(t);

                if(t != null){


                    Log.v(TAG, "Trip ID: " + t.getId());

                    start_date_search = t.getDate_start();

                    trip_date.setText(dateFormatTextView.format(t.getDate_start()));
                    try {
                        trip_time.setText(timeFormatTextView.format(timeFormatDataBase.parse(t.getTime_start())));
                        start_time_search = timeFormatDataBase.parse(t.getTime_start());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    trip_purpose.setText(t.getPurpose());
                    trip_from.setText(t.getFrom());
                    trip_to.setText(t.getTo());
                    trip_odometer_start.setText(Integer.toString(t.getOdometer_start()));
                    trip_odometer_end.setText(Integer.toString(t.getOdometer_end()));
                    trip_total_distance.setText(Integer.toString(t.getKm()) + " KM");
                    trip_total_amount.setText(Integer.toString(t.getAmount()));
                    trip_notes.setText(t.getNotes());

                    int trip_type_int = t.getType();

                    if(trip_type_int == TRIP_TYPE_BUSINESS){

                        trip_type.setSelection(TRIP_TYPE_BUSINESS);


                    }else if(trip_type_int == TRIP_TYPE_PRIVATE){

                        trip_type.setSelection(TRIP_TYPE_PRIVATE);

                    }


                }else{

                    save_button.setEnabled(false);
                    Toast.makeText(getApplicationContext(), "The ongoing trip couldn't be loaded", Toast.LENGTH_LONG).show();

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");


    }

    void deleteTripFromDB(){


        new AsyncTask<String, Void, String>(){

            private String message = "";

            @Override
            protected String doInBackground(String... params) {

                try {

                    AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                    database.tripDao().removeTrip(trip);
                    message = "success";
                    return message;


                } catch (Exception e) {
                    e.printStackTrace();
                    message = "failure";
                    return message;
                }

            }

            @Override
            protected void onPostExecute(String m) {
                super.onPostExecute(m);

                if(m.equals("success")){

                    try {

                        Log.v(TAG, "New trip removed in DB");
                        intent_ma.putExtra("RESULT_CODE", "TRIP_REQUEST_FINISHED");
                        getApplicationContext().sendBroadcast(intent_ma);
                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else{

                    Log.v(TAG, "Error on removing new trip in DB");

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");

    }

    public boolean isInternetOn(){

        WIFInetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        MOBILEnetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if((WIFInetInfo != null && WIFInetInfo.getState() == NetworkInfo.State.CONNECTED) || (MOBILEnetInfo != null && MOBILEnetInfo.getState() == NetworkInfo.State.CONNECTED)){

            return true;

        }else {

            return false;
        }

    }

    public void noInternetConnection(){

        Snackbar
                .make(findViewById(R.id.parent), "Geen internetverbinding", Snackbar.LENGTH_INDEFINITE)
                .setAction("Opnieuw proberen", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        save_button.performClick();
                    }
                })
                .show();
        save_button.setEnabled(true);

    }

    int getCurrentUserId(){


        String current_user = Preference.getString(getApplicationContext(), "current_user");


        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);

                return jsonObject_current_user.getInt("id");

            } catch (JSONException e) {


                e.printStackTrace();
            }


        }

        return 0;



    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public static class DatePickerFragment extends DialogFragment
    {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);

        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        Calendar cal = new GregorianCalendar(year, month, day);

        setDate(cal);

    }

    public void setDate(Calendar calendar) {

        //final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);


        try {

            //String short_format = dateFormatDataBase.format(calendar.getTime());
            //Log.v(TAG, "short format: " + dateFormatDataBase.parse(short_format).toString());

            start_date_search = calendar.getTime();
            trip_date.setText(dateFormatTextView.format(calendar.getTime()));

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static class TimePickerFragment extends DialogFragment
    {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            int second = c.get(Calendar.SECOND);

            // Create a new instance of DatePickerDialog and return it
            return new TimePickerDialog(getActivity(), (TimePickerDialog.OnTimeSetListener) getActivity(), hour, minute, false);

        }

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        Calendar t = Calendar.getInstance();

        t.set(Calendar.HOUR_OF_DAY, hourOfDay);
        t.set(Calendar.MINUTE, minute);
        setTime(t);
        //SimpleDateFormat mSDF = new SimpleDateFormat("hh:mm a");
        //hora = mSDF.format(datetime.getTime());

    }

    public void setTime(Calendar calendar) {

        //final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);


        try {

            //String short_format = dateFormatDataBase.format(calendar.getTime());
            //Log.v(TAG, "short format: " + dateFormatDataBase.parse(short_format).toString());
            start_time_search = calendar.getTime();
            trip_time.setText(timeFormatTextView.format(calendar.getTime()));

        } catch (Exception e) {
            e.printStackTrace();
        }


    }



}
