package proponomultimedia.citerio.com.trippoints.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.Date;
import java.util.List;

/**
 * Created by Jose Ricardo on 13/09/2017.
 */
@Dao
public interface TripDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addTrip(Trip trip);

    @Query("SELECT * FROM trip WHERE user = :id AND status = :status ORDER BY date_start DESC")
    List<Trip> getTrips(int id, int status);

    @Query("SELECT * FROM trip WHERE user = :id AND status IN (:status)")
    List<Trip> getTripsStatus(int id, int[] status);

    @Query("SELECT * FROM trip WHERE user = :id AND status = :status")
    Trip getTrip(int id, int status);

    @Query("SELECT * FROM trip WHERE user = :user_id AND id = :id")
    Trip getTripById(int user_id, int id);

    @Query("DELETE FROM trip")
    void removeAllTrips();

    @Query("SELECT * FROM trip WHERE user = :id AND status = :status AND date_start BETWEEN :from AND :to AND type = :type ORDER BY date_start")
    List<Trip> findTripsBetweenDates(int id, int status, Date from, Date to, int type);

    @Delete
    void removeTrip(Trip trip);

    @Update
    void updateTrip(Trip trip);

}
