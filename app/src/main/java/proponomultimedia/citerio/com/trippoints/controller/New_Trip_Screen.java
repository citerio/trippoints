package proponomultimedia.citerio.com.trippoints.controller;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.model.AppDatabase;
import proponomultimedia.citerio.com.trippoints.model.Car;
import proponomultimedia.citerio.com.trippoints.model.CarSpinnerAdapter;
import proponomultimedia.citerio.com.trippoints.model.LocationObject;
import proponomultimedia.citerio.com.trippoints.model.Preference;
import proponomultimedia.citerio.com.trippoints.model.Trip;
import proponomultimedia.citerio.com.trippoints.model.User;

/**
 * Created by Protinal on 02/02/2018.
 */

public class New_Trip_Screen extends AppCompatActivity {

    private Button cancel_button;
    private Button request_button;
    private ImageButton trip_from_button, trip_to_button;
    private EditText trip_purpose, trip_from, trip_to;
    private TextInputLayout trip_purpose_layout, trip_from_layout, trip_to_layout;
    private ProgressBar progressbar;
    private Spinner trip_type;
    private Spinner trip_car;
    private View selected_car;
    private TextView selected_brand_code;
    private JSONObject car_data = new JSONObject();
    private final int TRIP_TYPE_BUSINESS = 0;
    private final int TRIP_TYPE_PRIVATE = 1;
    private View decorView;
    private int uiOptions;
    private int TRIP_FROM = 1;
    private int TRIP_TO = 2;
    private static final String TAG = "MyActivity";
    private JSONObject data = new JSONObject();
    private ConnectivityManager cm;
    private NetworkInfo WIFInetInfo, MOBILEnetInfo;
    public static final String ACTION = "proponomultimedia.citerio.com.trippoints";
    private Intent intent_ma = new Intent(ACTION);
    private final int TRIP_PENDING = 1;
    private String url = "https://heel-and-toe-galley.000webhostapp.com/Main.php";
    private RequestQueue queue;
    private CarBroadCastReceiver carReceiver;
    private static final int REQUEST_EXIT = 567;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 ) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });

        setContentView(R.layout.new_trip_screen);

        carReceiver = new CarBroadCastReceiver();

        cancel_button = (Button)findViewById(R.id.cancel_button);
        request_button = (Button)findViewById(R.id.request_button);
        trip_from_button = (ImageButton) findViewById(R.id.trip_from_button);
        trip_to_button = (ImageButton) findViewById(R.id.trip_to_button);
        trip_purpose = (EditText) findViewById(R.id.trip_purpose);
        trip_from = (EditText) findViewById(R.id.trip_from);
        trip_to = (EditText) findViewById(R.id.trip_to);
        trip_type = (Spinner) findViewById(R.id.trip_type);
        trip_car = (Spinner) findViewById(R.id.trip_car);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        trip_purpose_layout = (TextInputLayout)findViewById(R.id.trip_purpose_layout);
        trip_from_layout = (TextInputLayout)findViewById(R.id.trip_from_layout);
        trip_to_layout = (TextInputLayout)findViewById(R.id.trip_to_layout);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> trip_type_adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.type_trip_export, R.layout.brand_item);
        // Specify the layout to use when the list of choices appears
        trip_type_adapter.setDropDownViewResource(R.layout.spinner_item);
        // Apply the adapter to the spinner
        trip_type.setAdapter(trip_type_adapter);

        cm = (ConnectivityManager)New_Trip_Screen.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        queue = Volley.newRequestQueue(this);

        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        request_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isInternetOn()){

                    try {

                        request_button.setEnabled(false);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                        String Date = dateFormat.format(new Date());

                        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                        String Time = timeFormat.format(new Date());

                        int user_id = getCurrentUserId();

                        data.put("operation", "trip_request");
                        data.put("date_start", Date);
                        data.put("date_end", Date);
                        data.put("time_start", Time);
                        data.put("time_end", Time);
                        data.put("purpose", trip_purpose.getText().toString().trim());
                        data.put("from", trip_from.getText().toString().trim());
                        data.put("to", trip_to.getText().toString().trim());
                        data.put("odometer_start", 0);
                        data.put("odometer_end", 0);

                        String trip_type_string = (String) trip_type.getSelectedItem();

                        if(trip_type_string.equals("Poslovanje")){

                            data.put("type", TRIP_TYPE_BUSINESS);


                        }else if(trip_type_string.equals("Privatna")){

                            data.put("type", TRIP_TYPE_PRIVATE);

                        }

                        data.put("distance", 0);
                        data.put("amount", 0);
                        data.put("notes", "");
                        data.put("user_id", user_id);
                        data.put("user_name", getCurrentUserName());
                        data.put("status", TRIP_PENDING);

                        selected_car = trip_car.getSelectedView();

                        car_data.put("id", ((TextView) selected_car.findViewById(R.id.id)).getText().toString());
                        car_data.put("name", ((TextView) selected_car.findViewById(R.id.name)).getText().toString());
                        car_data.put("plate", ((TextView) selected_car.findViewById(R.id.plate)).getText().toString());
                        car_data.put("manufacturer", ((TextView) selected_car.findViewById(R.id.manufacturer)).getText().toString());

                        data.put("car", car_data.toString());

                        sendToServer(data.toString());

                    }catch (Exception e){

                        e.printStackTrace();

                    }


                }else {

                    //loginOffline();
                    noInternetConnection();

                }



            }
        });

        trip_from_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findPlaceFrom();
            }
        });

        trip_to_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findPlaceTo();
            }
        });

        trip_from.addTextChangedListener(new New_Trip_Screen.MyTextWatcher(trip_from));
        trip_to.addTextChangedListener(new New_Trip_Screen.MyTextWatcher(trip_to));
        trip_purpose.addTextChangedListener(new New_Trip_Screen.MyTextWatcher(trip_purpose));

        loadSpinner();

    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }

    private class MyTextWatcher implements TextWatcher {


        private View view;

        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (view.getId()){
                case R.id.trip_from:
                    validate_from();
                    break;
                case R.id.trip_to:
                    validate_to();
                    break;
                case R.id.trip_purpose:
                    validate_purpose();
                    break;
            }

        }
    }

    private boolean validate_purpose(){

        if(trip_purpose.getText().toString().trim().isEmpty()){

            trip_purpose_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            trip_purpose_layout.setErrorEnabled(false);

        }

        return true;

    }

    private boolean validate_to(){

        if(trip_to.getText().toString().trim().isEmpty()){

            trip_to_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            trip_to_layout.setErrorEnabled(false);

        }

        return true;

    }

    private boolean validate_from(){

        if(trip_from.getText().toString().trim().isEmpty()){

            trip_from_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            trip_from_layout.setErrorEnabled(false);

        }

        return true;

    }

    public void findPlaceFrom() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            //.setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, TRIP_FROM);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void findPlaceTo() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            //.setFilter(typeFilter)
                            .build(this);
            startActivityForResult(intent, TRIP_TO);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    // A place has been received; use requestCode to track the request.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TRIP_FROM) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                trip_from.setText(place.getName());
                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        if (requestCode == TRIP_TO) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                trip_to.setText(place.getName());
                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        if (requestCode == REQUEST_EXIT) {
            if (resultCode == RESULT_OK) {
                finish();

            }
        }
    }


    public void sendToServer(String data){

        progressbar.setVisibility(View.VISIBLE);

        Map<String, String> params_m = new HashMap<String, String>();

        params_m.put("data", data);



        JSONObject params = new JSONObject(params_m);
        /*try {
            params.put("data", );
        } catch (JSONException e) {
            e.printStackTrace();
        }*/



        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {

                        progressbar.setVisibility(View.INVISIBLE);

                        try {

                            if(response.getString("result").equals("success")){


                                new AsyncTask<String, Void, String>(){


                                    private String trip;
                                    private String message = "";

                                    @Override
                                    protected String doInBackground(String... params) {

                                        trip = params[0];

                                        try {

                                            JSONObject trip_data = new JSONObject(trip);

                                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                            Date date = dateFormat.parse(trip_data.getString("date_start"));

                                            Trip trip;
                                            JSONObject car_data = new JSONObject(trip_data.getString("car"));
                                            trip =  new Trip(trip_data.getInt("id"), date, date, trip_data.getString("time_start"), trip_data.getString("time_end"), trip_data.getString("purpose"), trip_data.getString("from_t"), trip_data.getString("to"), trip_data.getInt("odometer_start"), trip_data.getInt("odometer_end"), trip_data.getInt("type"), trip_data.getInt("distance"), trip_data.getInt("amount"), trip_data.getString("notes"), trip_data.getInt("user_id"), TRIP_PENDING, "", car_data.getInt("id"));
                                            AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                                            database.tripDao().addTrip(trip);

                                            message = "success";
                                            return message;


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            message = "failure";
                                            return message;
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            message = "failure";
                                            return message;
                                        }


                                    }

                                    @Override
                                    protected void onPostExecute(String m) {
                                        super.onPostExecute(m);

                                        if(m.equals("success")){

                                            try {

                                                Log.v(TAG, "New trip inserted in DB");
                                                intent_ma.putExtra("RESULT_CODE", "TRIP_REQUEST_PENDING");
                                                getApplicationContext().sendBroadcast(intent_ma);
                                                Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                                finish();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }else{

                                            Log.v(TAG, "Error on inserting new trip in DB");
                                            Toast.makeText(getApplicationContext(), "ERROR inside adding trip", Toast.LENGTH_LONG).show();

                                        }

                                    }
                                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, response.getString("trip"));


                            }else if(response.getString("result").equals("failure")){

                                //Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                Snackbar
                                        .make(findViewById(R.id.parent), response.getString("message"), Snackbar.LENGTH_INDEFINITE)
                                        .setAction("Retry", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                request_button.performClick();
                                            }
                                        })
                                        .show();
                                request_button.setEnabled(true);

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.setVisibility(View.INVISIBLE);
                request_button.setEnabled(true);
                Toast.makeText(getApplicationContext(), "ERROR on JsonRequest"+error.toString(), Toast.LENGTH_LONG).show();
            }

        }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        // Add the request to the RequestQueue.
        queue.add(request);

    }


    /*void addTripToDB(String data){


        new AsyncTask<String, Void, String>(){

            private String message = "";
            private String data = "";

            @Override
            protected String doInBackground(String... params) {

                try {

                    data = params[0];

                    JSONObject trip_data = new JSONObject(data);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    Date date = dateFormat.parse(trip_data.getString("date_start"));

                    Trip trip;
                    trip =  new Trip(0, date, date, trip_data.getString("time_start"), trip_data.getString("time_end"), trip_data.getString("purpose"), trip_data.getString("from"), trip_data.getString("to"), trip_data.getInt("odometer_start"), trip_data.getInt("odometer_end"), trip_data.getInt("type"), trip_data.getInt("km"), trip_data.getInt("amount"), trip_data.getString("notes"), trip_data.getInt("user_id"), trip_data.getInt("status"), "", 0);
                    AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                    database.tripDao().addTrip(trip);
                    message = "success";
                    return message;


                } catch (Exception e) {
                    e.printStackTrace();
                    message = "failure";
                    return message;
                }

            }

            @Override
            protected void onPostExecute(String m) {
                super.onPostExecute(m);

                if(m.equals("success")){

                    try {

                        Log.v(TAG, "New trip inserted in DB");
                        intent_ma.putExtra("RESULT_CODE", "TRIP_REQUEST_PENDING");
                        getApplicationContext().sendBroadcast(intent_ma);
                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else{

                    Log.v(TAG, "Error on inserting new trip in DB");

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);

    }*/

    public boolean isInternetOn(){

        WIFInetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        MOBILEnetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if((WIFInetInfo != null && WIFInetInfo.getState() == NetworkInfo.State.CONNECTED) || (MOBILEnetInfo != null && MOBILEnetInfo.getState() == NetworkInfo.State.CONNECTED)){

            return true;

        }else {

            return false;
        }

    }

    public void noInternetConnection(){

        Snackbar
                .make(findViewById(R.id.parent), "Geen internetverbinding", Snackbar.LENGTH_INDEFINITE)
                .setAction("Opnieuw proberen", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        request_button.performClick();
                    }
                })
                .show();
        request_button.setEnabled(true);

    }

     String getCurrentUserName(){


        String current_user = Preference.getString(getApplicationContext(), "current_user");


        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);

                return jsonObject_current_user.getString("name");

            } catch (JSONException e) {


                e.printStackTrace();
            }


        }

        return "";



    }

    int getCurrentUserId(){


        String current_user = Preference.getString(getApplicationContext(), "current_user");


        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);

                return jsonObject_current_user.getInt("id");

            } catch (JSONException e) {


                e.printStackTrace();
            }


        }

        return 0;



    }

    public void loadSpinner(){


        new AsyncTask<Spinner, Void, CarSpinnerAdapter>() {


            private CarSpinnerAdapter car_adapter = null;
            private Spinner v;

            @Override
            protected CarSpinnerAdapter doInBackground(Spinner... params) {

                try {

                    v = params[0];

                    AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                    car_adapter = new CarSpinnerAdapter(getApplicationContext(), (ArrayList<Car>) database.carDao().getCars(getCurrentUserId()));
                    return car_adapter;

                }catch (Exception e){

                    e.printStackTrace();
                    return null;

                }


            }

            @Override
            protected void onPostExecute(CarSpinnerAdapter s) {
                super.onPostExecute(s);

                if(car_adapter != null){

                    car_adapter.setDropDownViewResource(R.layout.spinner_item);

                    v.setAdapter(car_adapter);

                    if(v.getAdapter().getCount() == 0){

                        showDialog();
                    }


                }else{

                    Toast.makeText(getApplicationContext(), "car spinner adapter null",  Toast.LENGTH_LONG).show();


                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, trip_car);



    }

    private class CarBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String local = intent.getExtras().getString("RESULT_CODE");
            assert local != null;
            if(local.equals("NEW_CAR_ADDED")){
                //get all data from database
                loadSpinner();

            }
        }
    }

    @Override
    public void onStart() {
        getApplicationContext().registerReceiver(carReceiver, new IntentFilter("proponomultimedia.citerio.com.trippoints"));
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getApplicationContext().unregisterReceiver(carReceiver);
    }

    public void showDialog(){

        AppCompatDialogFragment newFragment = MyAlertDialogFragment.newInstance(
                0);
        newFragment.show(getSupportFragmentManager(), "dialog");

    }


    public static class MyAlertDialogFragment extends AppCompatDialogFragment {

        public static MyAlertDialogFragment newInstance(int title) {
            MyAlertDialogFragment frag = new MyAlertDialogFragment();
            Bundle args = new Bundle();
            args.putInt("title", title);
            frag.setArguments(args);
            return frag;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int title = getArguments().getInt("title");

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            return builder
                    .setIcon(R.mipmap.ic_info_outline_black_36dp)
                    .setTitle(R.string.information)
                    .setMessage(R.string.no_cars_recorded)
                    .setCancelable(false)
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            getActivity().finish();

                        }
                    })

                    .setPositiveButton(R.string.add_car, new DialogInterface.OnClickListener() {


                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            Intent new_car_screen = new Intent(getActivity(), New_Car_Screen.class);
                            getActivity().startActivityForResult(new_car_screen, REQUEST_EXIT);

                        }
                    })
                    .create();
        }
    }




}
