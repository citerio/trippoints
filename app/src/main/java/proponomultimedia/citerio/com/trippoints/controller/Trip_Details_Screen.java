package proponomultimedia.citerio.com.trippoints.controller;

import android.content.Intent;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.model.AppDatabase;
import proponomultimedia.citerio.com.trippoints.model.Car;
import proponomultimedia.citerio.com.trippoints.model.Preference;
import proponomultimedia.citerio.com.trippoints.model.Trip;

public class Trip_Details_Screen extends AppCompatActivity {private Button cancel_button;
    private Button edit_button;
    private TextView trip_total_distance, trip_total_amount;
    private TextView trip_purpose, trip_from, trip_to, trip_date, trip_car, trip_time, trip_odometer_start, trip_odometer_end, trip_notes, trip_type;
    private ImageView map;
    private ProgressBar progressbar;
    private View decorView;
    private int uiOptions;
    private final int TRIP_TYPE_BUSINESS = 0;
    private final int TRIP_TYPE_PRIVATE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 ) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });
        setContentView(R.layout.trip_details_screen);

        cancel_button = (Button)findViewById(R.id.cancel_button);
        edit_button = (Button)findViewById(R.id.edit_button);
        trip_purpose = (TextView) findViewById(R.id.trip_purpose);
        trip_from = (TextView) findViewById(R.id.trip_from);
        trip_to = (TextView) findViewById(R.id.trip_to);
        trip_date = (TextView) findViewById(R.id.trip_date);
        //trip_time = (EditText) findViewById(R.id.trip_time);
        trip_notes = (TextView) findViewById(R.id.trip_notes);
        trip_odometer_start = (TextView) findViewById(R.id.trip_odometer_start);
        trip_odometer_end = (TextView) findViewById(R.id.trip_odometer_end);
        trip_total_distance = (TextView) findViewById(R.id.trip_total_distance);
        trip_total_amount = (TextView) findViewById(R.id.trip_total_amount);
        trip_type = (TextView) findViewById(R.id.trip_type);
        trip_car = (TextView) findViewById(R.id.trip_car);
        map = (ImageView) findViewById(R.id.map);

        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent edit_trip_screen = new Intent(Trip_Details_Screen.this, Edit_Trip_Screen.class);
                edit_trip_screen.putExtra("trip_id", getIntent().getIntExtra("trip_id", 0));
                startActivity(edit_trip_screen);
            }
        });


        getTripFromDB();

    }

    void getTripFromDB(){


        new AsyncTask<String, Void, Trip>(){

            private Car car;


            @Override
            protected Trip doInBackground(String... params) {

                try{

                    int trip_id = getIntent().getIntExtra("trip_id", 0);

                    AppDatabase database = AppDatabase.getDatabase(getApplicationContext());

                    Trip trip = database.tripDao().getTripById(getCurrentUserId(), trip_id);

                    if(trip != null){

                        car = database.carDao().getCar(getCurrentUserId(), trip.getCar());

                    }

                    return trip;

                }catch (Exception e){

                    e.printStackTrace();
                    return null;

                }


            }

            @Override
            protected void onPostExecute(Trip t) {
                super.onPostExecute(t);

                if(t != null){

                    if( t.getMap() != null){
                        Picasso.with(getApplicationContext()).load(new File(t.getMap())).fit().centerCrop().error(R.mipmap.logo).into(map);
                    }

                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);

                    trip_date.setText(dateFormat.format(t.getDate_start()));
                    //trip_time.setText(t.getTime_start());
                    trip_purpose.setText(t.getPurpose());
                    trip_from.setText(t.getFrom());
                    trip_to.setText(t.getTo());
                    trip_odometer_start.setText(Integer.toString(t.getOdometer_start()) + " KM");
                    trip_odometer_end.setText(Integer.toString(t.getOdometer_end()) + " KM");
                    trip_total_distance.setText(Integer.toString(t.getKm()) + " KM");
                    trip_total_amount.setText(Integer.toString(t.getAmount()));
                    trip_notes.setText(t.getNotes());

                    int trip_type_int = t.getType();

                    if(trip_type_int == TRIP_TYPE_BUSINESS){

                        trip_type.setText("Poslovanje");


                    }else if(trip_type_int == TRIP_TYPE_PRIVATE){

                        trip_type.setText("Privatna");

                    }

                    if (car != null){

                        trip_car.setText(car.getName() + " " + car.getManufacturer());

                    }

                    //Log.v("Trip Points console: ", "map " + t.getMap());



                }else{

                    Toast.makeText(getApplicationContext(), "The trip couldn't be loaded", Toast.LENGTH_LONG).show();

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");


    }

    int getCurrentUserId(){


        String current_user = Preference.getString(getApplicationContext(), "current_user");


        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);

                return jsonObject_current_user.getInt("id");

            } catch (JSONException e) {


                e.printStackTrace();
            }


        }

        return 0;



    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }
}
