package proponomultimedia.citerio.com.trippoints.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Protinal on 27/04/2018.
 */

@Entity
public class Car {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int user_id;
    private String name;
    private String manufacturer;
    private String plate;

    public Car(int user_id, String name, String manufacturer, String plate) {
        this.user_id = user_id;
        this.name = name;
        this.manufacturer = manufacturer;
        this.plate = plate;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    @Override
    public String toString() {
        return name;
    }
}
