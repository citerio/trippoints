package proponomultimedia.citerio.com.trippoints.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Jose Ricardo on 13/09/2017.
 */
@Dao
public interface LocationObjectDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addLocationObject(LocationObject location);

    @Query("SELECT * FROM locationobject")
    List<LocationObject> getAllLocations();

    @Query("DELETE FROM locationobject")
    void removeAllLocations();
}
