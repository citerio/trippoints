package proponomultimedia.citerio.com.trippoints.controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.model.AppDatabase;
import proponomultimedia.citerio.com.trippoints.model.User;

public class Confirm_Email_Screen extends AppCompatActivity {

    private Button confirm_email_button;
    private ProgressBar progressbar;
    private View decorView;
    private int uiOptions;
    private TextInputLayout code_layout;
    private EditText code;
    private ConnectivityManager cm;
    private NetworkInfo WIFInetInfo, MOBILEnetInfo;
    private JSONObject data = new JSONObject();
    private RequestQueue queue;
    private String url = "https://heel-and-toe-galley.000webhostapp.com/Main.php";
    private String url_main = "https://portal.shapeview.nl/";
    private String email = "";
    private String password = "";
    private static final String PROPERTY_REG_ID = "reg_id";
    private String regId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent create_acoount_screen = getIntent();
        email = create_acoount_screen.getStringExtra("email");
        password = create_acoount_screen.getStringExtra("password");

        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0 ) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });

        setContentView(R.layout.confirm_email_screen);

        confirm_email_button = (Button)findViewById(R.id.confirm_email_button);
        code_layout = (TextInputLayout)findViewById(R.id.code_layout);
        code = (EditText)findViewById(R.id.code);
        progressbar = (ProgressBar)findViewById(R.id.progressbar);

        cm = (ConnectivityManager)Confirm_Email_Screen.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        queue = Volley.newRequestQueue(this);

        code.addTextChangedListener(new Confirm_Email_Screen.MyTextWatcher(code));

        confirm_email_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(isInternetOn()){

                    try {

                        confirm_email_button.setEnabled(false);

                        regId = getRegistrationId(getApplicationContext());

                        data.put("operation", "confirm_email");
                        data.put("code", code.getText().toString());
                        data.put("email", email);
                        data.put("password", password);
                        data.put("token", regId);

                        confirmEmail(data.toString());

                    }catch (Exception e){

                        e.printStackTrace();

                    }


                }else {

                    //loginOffline();
                    noInternetConnection();

                }

                //Intent home_view = new Intent(Logging_System.this, Home_View.class);
                //startActivity(home_view);



            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }

    private class MyTextWatcher implements TextWatcher {


        private View view;

        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (view.getId()){
                case R.id.code:
                    validate_code();
                    break;
            }

        }
    }

    private boolean validate_code(){

        if(code.getText().toString().trim().isEmpty()){

            code_layout.setError(getString(R.string.error_empty_field));
            return false;

        }else{

            code_layout.setErrorEnabled(false);

        }

        return true;

    }

    public void confirmEmail(String data){

        progressbar.setVisibility(View.VISIBLE);

        Map<String, String> params_m = new HashMap<String, String>();

        params_m.put("data", data);

        JSONObject params = new JSONObject(params_m);

       /* JSONObject params = new JSONObject();
        try {
            params.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {

                        progressbar.setVisibility(View.INVISIBLE);

                        try {

                            if(response.getString("result").equals("success")){


                                new AsyncTask<String, Void, String>(){


                                    private String user;
                                    private String message = "";

                                    @Override
                                    protected String doInBackground(String... params) {

                                        user = params[0];

                                        try {

                                            JSONObject current_user =  new JSONObject(user);
                                            User user;
                                            user = new User(current_user.getInt("id"), current_user.getString("name"), current_user.getString("company"), current_user.getString("address"), current_user.getString("phone_number"), current_user.getString("email"), current_user.getString("password"), current_user.getString("token"));
                                            AppDatabase database = AppDatabase.getDatabase(getApplicationContext());
                                            database.userDao().addUser(user);
                                            saveCurrentUser(current_user.getString("name"), current_user.getString("email"), current_user.getInt("id"),true);
                                            message = "success";
                                            return message;


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            message = "failure";
                                            return message;
                                        }




                                    }

                                    @Override
                                    protected void onPostExecute(String m) {
                                        super.onPostExecute(m);

                                        if(m.equals("success")){

                                            try {

                                                Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                                Intent employee_tabs_screen = new Intent(Confirm_Email_Screen.this, Employee_Tabs_Screen.class);
                                                startActivity(employee_tabs_screen);
                                                finish();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }else{

                                            Toast.makeText(getApplicationContext(), "ERROR inside adding user", Toast.LENGTH_LONG).show();

                                        }

                                    }
                                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, response.getString("user"));


                            }else if(response.getString("result").equals("failure")){

                                //Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                                Snackbar
                                        .make(findViewById(R.id.parent), response.getString("message"), Snackbar.LENGTH_INDEFINITE)
                                        .setAction("Retry", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                confirm_email_button.performClick();
                                            }
                                        })
                                        .show();
                                confirm_email_button.setEnabled(true);

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }




                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.setVisibility(View.INVISIBLE);
                confirm_email_button.setEnabled(true);
                Toast.makeText(getApplicationContext(), "ERROR on JsonRequest"+error.toString(), Toast.LENGTH_LONG).show();
            }

        }
        )
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

        };
        // Add the request to the RequestQueue.
        queue.add(request);

    }

    public boolean isInternetOn(){

        WIFInetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        MOBILEnetInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if((WIFInetInfo != null && WIFInetInfo.getState() == NetworkInfo.State.CONNECTED) || (MOBILEnetInfo != null && MOBILEnetInfo.getState() == NetworkInfo.State.CONNECTED)){

            return true;

        }else {

            return false;
        }

    }

    /*public void loginOffline(){


        new AsyncTask<String, Void, User>(){


            private User user;
            private String email;
            private String password;

            @Override
            protected User doInBackground(String... params) {

                email = params[0];
                password = params[1];

                AppDatabase database = AppDatabase.getDatabase(getApplicationContext());

                user = database.userDao().getUser(email, password);

                return user;
            }

            @Override
            protected void onPostExecute(User u) {
                super.onPostExecute(u);

                if(u != null){

                    saveCurrentUser(u.getEmail(), u.getId(), u.getToken(), u.getHome_title(), u.getHome_logo(), true);


                }else{

                    //Toast.makeText(getApplicationContext(), "Wrong Credentials", Toast.LENGTH_LONG).show();
                    Snackbar
                            .make(findViewById(R.id.parent), "Wrong Credentials", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    signin_button.performClick();
                                }
                            })
                            .show();
                    signin_button.setEnabled(true);

                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, email.getText().toString().trim(), password.getText().toString().trim());


    }*/

    /*void saveCurrentUser(String email, int id, String token, String home_title, String home_logo, boolean logged){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String current_user = prefs.getString("current_user", "");
        SharedPreferences.Editor editor = prefs.edit();

        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);
                jsonObject_current_user.put("email", email);
                jsonObject_current_user.put("id", id);
                jsonObject_current_user.put("token", token);
                jsonObject_current_user.put("home_title", home_title);
                jsonObject_current_user.put("home_logo", home_logo);
                jsonObject_current_user.put("logged", logged);

                editor.putString("current_user", jsonObject_current_user.toString());
                editor.apply();

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


    }*/

    public void noInternetConnection(){

        Snackbar
                .make(findViewById(R.id.parent), "Geen internetverbinding", Snackbar.LENGTH_INDEFINITE)
                .setAction("Opnieuw proberen", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirm_email_button.performClick();
                    }
                })
                .show();
        confirm_email_button.setEnabled(true);

    }

    void saveCurrentUser(String name, String email, int id, boolean logged){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String current_user = prefs.getString("current_user", "");
        SharedPreferences.Editor editor = prefs.edit();

        if(!current_user.isEmpty()){

            try {

                JSONObject jsonObject_current_user = new JSONObject(current_user);
                jsonObject_current_user.put("name", name);
                jsonObject_current_user.put("email", email);
                jsonObject_current_user.put("id", id);
                jsonObject_current_user.put("logged", logged);

                editor.putString("current_user", jsonObject_current_user.toString());
                editor.apply();

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


    }

    ///fetching the registration id
    private String getRegistrationId(Context context) throws Exception {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }

        return registrationId;
    }

}
