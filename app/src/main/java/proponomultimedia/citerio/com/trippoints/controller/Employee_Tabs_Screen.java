package proponomultimedia.citerio.com.trippoints.controller;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;


import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.BuildConfig;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPTableHeader;
import com.itextpdf.text.pdf.PdfWriter;
import com.opencsv.CSVWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import proponomultimedia.citerio.com.trippoints.model.AppDatabase;
import proponomultimedia.citerio.com.trippoints.model.LocationObject;
import proponomultimedia.citerio.com.trippoints.model.Preference;
import proponomultimedia.citerio.com.trippoints.model.Trip;
import proponomultimedia.citerio.com.trippoints.model.TripAdapter;
import proponomultimedia.citerio.com.trippoints.R;
import proponomultimedia.citerio.com.trippoints.utility.RouteService;

public class Employee_Tabs_Screen extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */


    /**
     * The {@link ViewPager} that will host the section contents.
     */

    private BottomNavigationView navigation;
    private View decorView;
    private int uiOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        decorView = getWindow().getDecorView();
        uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE;

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    // TODO: The system bars are visible. Make any desired
                    decorView.setSystemUiVisibility(uiOptions);

                }
            }
        });

        setContentView(R.layout.employee_tabs_screen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        setupBottomNavigation();

        if (savedInstanceState == null) {

            loadTripFragment();
        }

    }


    public void loadTripFragment() {

        TripFragment fragment = TripFragment.newInstance(1);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void loadBookFragment() {

        BookFragment fragment = BookFragment.newInstance(2);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void loadExportFragment() {

        ExportFragment fragment = ExportFragment.newInstance(3);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void loadSettingsFragment() {

        SettingsFragment fragment = SettingsFragment.newInstance(4);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void loadAboutFragment() {

        AboutFragment fragment = AboutFragment.newInstance(5);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void setupBottomNavigation() {

        navigation = (BottomNavigationView) findViewById(R.id.navigation);


        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.trip:
                        loadTripFragment();
                        return true;
                    case R.id.book:
                        loadBookFragment();
                        return true;
                    case R.id.export:
                        loadExportFragment();
                        return true;
                    case R.id.settings:
                        loadSettingsFragment();
                        return true;
                    case R.id.about:
                        loadAboutFragment();
                        return true;
                }
                return false;
            }
        });
    }


    /**
     * A trip fragment containing a simple view.
     */
    public static class TripFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        public static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 123;
        public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 456;
        private View rootView;
        private GoogleMap mMap;
        private LatLng place;
        private ViewGroup containerView;
        private FloatingActionButton start_trip_button, trip_request_button, stop_trip_button;
        private static final String LOG = "MyActivity";
        private GoogleApiClient mGoogleApiClient;
        private Location mLastLocation;
        private LocationRequest mLocationRequest;
        private double latitudeValue = 0.0;
        private double longitudeValue = 0.0;
        private RouteBroadCastReceiver routeReceiver;
        private List<LocationObject> startToPresentLocations;
        private static final String TAG = "MyActivity";
        private Intent streamService;
        private final int REQUEST_CHECK_SETTINGS = 300;
        private final int TRIP_PENDING = 1;
        private final int TRIP_APPROVED = 2;
        private final int TRIP_ONGOING = 3;
        private final int TRIP_FINISHED = 4;
        private CardView trip_notification_approved, trip_notification_pending;
        private Button see_details_button;
        private Trip trip;
        private String map_path = "";


        public TripFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static TripFragment newInstance(int sectionNumber) {
            TripFragment fragment = new TripFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            //Log.v(LOG, "onCreate radio called");
            streamService = new Intent(getActivity().getApplicationContext(), RouteService.class);

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_trip_tabs_screen, container, false);
            containerView = container;
            start_trip_button = (FloatingActionButton) rootView.findViewById(R.id.start_button);
            stop_trip_button = (FloatingActionButton) rootView.findViewById(R.id.stop_button);
            trip_request_button = (FloatingActionButton) rootView.findViewById(R.id.trip_request_button);
            trip_notification_approved = (CardView) rootView.findViewById(R.id.trip_notification_approved);
            trip_notification_pending = (CardView) rootView.findViewById(R.id.trip_notification_pending);
            see_details_button = (Button) rootView.findViewById(R.id.see_datails_button);


            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(getActivity().getApplicationContext())
                        .addConnectionCallbacks(this)
                        .addApi(LocationServices.API)
                        .build();
            }

            startToPresentLocations = new ArrayList<LocationObject>();

            mLocationRequest = createLocationRequest();
            routeReceiver = new RouteBroadCastReceiver();

            trip_request_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent new_trip_screen = new Intent(getActivity(), New_Trip_Screen.class);
                    startActivity(new_trip_screen);
                }
            });

            start_trip_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getActivity().startService(streamService);
                    Preference.saveBoolean(getActivity(), "service_status", true);
                    Preference.saveInt(getActivity(), "current_trip_status", TRIP_ONGOING);
                    updateCurrentTripStatusToDB();

                }
            });

            stop_trip_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getActivity().stopService(streamService);
                    Preference.saveBoolean(getActivity(), "service_status", false);
                    Preference.saveInt(getActivity(), "current_trip_status", TRIP_FINISHED);
                    if(checkPermission(MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, "Write External Storage Permission needed, please grant")){

                        captureMapsScreen();

                    }
                    //tripStatusChecking();
                }
            });

            see_details_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent trip_pending_details_screen = new Intent(getActivity(), Trip_Pending_Details_Screen.class);
                    startActivity(trip_pending_details_screen);

                }
            });

            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

            mapFragment.getMapAsync(this);

            getCurrentTripStatusFromDB();

            return rootView;
        }


        void tripStatusChecking(int status){

            if(status == TRIP_PENDING){

                trip_notification_pending.setVisibility(View.VISIBLE);
                trip_notification_approved.setVisibility(View.GONE);
                trip_request_button.setVisibility(View.GONE);
                start_trip_button.setVisibility(View.GONE);
                stop_trip_button.setVisibility(View.GONE);

            }

            if(status == TRIP_APPROVED){

                trip_notification_pending.setVisibility(View.GONE);
                trip_notification_approved.setVisibility(View.VISIBLE);
                trip_request_button.setVisibility(View.GONE);
                start_trip_button.setVisibility(View.VISIBLE);
                stop_trip_button.setVisibility(View.GONE);

            }

            if(status == TRIP_ONGOING){

                trip_notification_pending.setVisibility(View.GONE);
                trip_notification_approved.setVisibility(View.GONE);
                trip_request_button.setVisibility(View.GONE);
                start_trip_button.setVisibility(View.GONE);
                stop_trip_button.setVisibility(View.VISIBLE);

                getAllLocationObjectsfromDB();

            }

            if(status == TRIP_FINISHED){

                trip_notification_pending.setVisibility(View.GONE);
                trip_notification_approved.setVisibility(View.GONE);
                trip_request_button.setVisibility(View.VISIBLE);
                start_trip_button.setVisibility(View.GONE);
                stop_trip_button.setVisibility(View.GONE);

            }



        }

        @Override
        public void onMapReady(GoogleMap map) {

            mMap = map;

        }

        private void markStartingLocationOnMap(GoogleMap mapObject, LatLng location){
            mapObject.addMarker(new MarkerOptions().position(location).title("Start Position"));
            mapObject.moveCamera(CameraUpdateFactory.newLatLng(location));
        }

        @Override
        public void onConnected(@Nullable Bundle bundle) {
            Log.d(TAG, "Connection method has been called");
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(@NonNull LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            int trip_status = Preference.getInt(getActivity(), "current_trip_status");
                            if(trip_status != TRIP_ONGOING){

                                getLastLocation();

                            }
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied, but this can be fixed
                            // by showing the user a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            });
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            switch (requestCode) {
                case REQUEST_CHECK_SETTINGS:
                    switch (resultCode) {
                        case Activity.RESULT_OK:
                            int trip_status = Preference.getInt(getActivity(), "current_trip_status");
                            if(trip_status != TRIP_ONGOING){

                                getLastLocation();

                            }
                            break;
                        case Activity.RESULT_CANCELED:
                            // user does not want to update setting. Handle it in a way that it will to affect your app functionality
                            Toast.makeText(getActivity(), "User does not update location setting", Toast.LENGTH_LONG).show();
                            break;
                    }
                    break;
            }
        }

        @Override
        public void onConnectionSuspended(int i) {
        }

        private class RouteBroadCastReceiver extends BroadcastReceiver{
            @Override
            public void onReceive(Context context, Intent intent) {
                String local = intent.getExtras().getString("RESULT_CODE");
                assert local != null;
                if(local.equals("GPS_NEW_LOCATION")){
                    //get all data from database
                    getAllLocationObjectsfromDB();

                }
                if(local.equals("TRIP_REQUEST_PENDING")){
                    //get all data from database
                    getCurrentTripStatusFromDB();

                }
                if(local.equals("TRIP_REQUEST_APPROVED")){
                    //get all data from database
                    getCurrentTripStatusFromDB();

                }
                if(local.equals("TRIP_REQUEST_FINISHED")){
                    //get all data from database
                    getCurrentTripStatusFromDB();

                }
            }
        }

        void getAllLocationObjectsfromDB(){


            new AsyncTask<String, Void, String>(){

                private String message = "";

                @Override
                protected String doInBackground(String... params) {

                    try {

                        AppDatabase database = AppDatabase.getDatabase(getActivity().getApplicationContext());
                        startToPresentLocations = database.locationObjectDao().getAllLocations();
                        message = "success";
                        return message;


                    } catch (Exception e) {
                        e.printStackTrace();
                        message = "failure";
                        return message;
                    }

                }

                @Override
                protected void onPostExecute(String m) {
                    super.onPostExecute(m);

                    if(m.equals("success")){

                        try {

                            if(startToPresentLocations.size() > 0){
                                //prepare map drawing.
                                List<LatLng> locationPoints = getPoints(startToPresentLocations);
                                refreshMap(mMap);
                                markStartingLocationOnMap(mMap, locationPoints.get(0));
                                drawRouteOnMap(mMap, locationPoints);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }else{

                        Toast.makeText(getActivity().getApplicationContext(), "ERROR inside getting locations from DB", Toast.LENGTH_LONG).show();

                    }

                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");

        }

        void removeAllLocationsfromDB(){


            new AsyncTask<String, Void, String>(){

                private String message = "";

                @Override
                protected String doInBackground(String... params) {

                    try {

                        AppDatabase database = AppDatabase.getDatabase(getActivity().getApplicationContext());
                        database.locationObjectDao().removeAllLocations();
                        message = "success";
                        return message;


                    } catch (Exception e) {
                        e.printStackTrace();
                        message = "failure";
                        return message;
                    }

                }

                @Override
                protected void onPostExecute(String m) {
                    super.onPostExecute(m);

                    if(m.equals("success")){

                        try {

                            getLastLocation();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }else{

                        Toast.makeText(getActivity().getApplicationContext(), "ERROR inside removing locations from DB", Toast.LENGTH_LONG).show();

                    }

                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");

        }

        int getCurrentUserId(){


            String current_user = Preference.getString(getActivity(), "current_user");


            if(!current_user.isEmpty()){

                try {

                    JSONObject jsonObject_current_user = new JSONObject(current_user);

                    return jsonObject_current_user.getInt("id");

                } catch (JSONException e) {


                    e.printStackTrace();
                }


            }

            return 0;



        }

        void getCurrentTripStatusFromDB(){


            new AsyncTask<String, Void, List<Trip>>(){


                @Override
                protected List<Trip> doInBackground(String... params) {

                    int[] statuses = new int[3];
                    statuses[0] = TRIP_PENDING;
                    statuses[1] = TRIP_APPROVED;
                    statuses[2] = TRIP_ONGOING;

                    AppDatabase database = AppDatabase.getDatabase(getActivity().getApplicationContext());

                    List<Trip> trips = database.tripDao().getTripsStatus(getCurrentUserId(), statuses);

                    return trips;
                }

                @Override
                protected void onPostExecute(List<Trip> t) {
                    super.onPostExecute(t);

                    if(t != null){

                        if(t.size() == 0 ){

                            tripStatusChecking(TRIP_FINISHED);

                        }else if(t.size() == 1){

                            tripStatusChecking(t.get(0).getStatus());

                        }else if(t.size() > 1){

                            Toast.makeText(getActivity().getApplicationContext(), "The trip statuses is inconsistent", Toast.LENGTH_LONG).show();

                        }


                    }else{

                        Toast.makeText(getActivity().getApplicationContext(), "The trip statuses couldn't be loaded", Toast.LENGTH_LONG).show();

                    }

                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");


        }

        void updateCurrentTripStatusToDB(){


            new AsyncTask<String, Void, String>(){

                private String message = "";


                @Override
                protected String doInBackground(String... params) {

                    try {

                        AppDatabase database = AppDatabase.getDatabase(getActivity().getApplicationContext());

                        Trip trip = database.tripDao().getTrip(getCurrentUserId(), TRIP_APPROVED);

                        if(trip != null){

                            trip.setStatus(TRIP_ONGOING);
                            database.tripDao().updateTrip(trip);
                            message = "success";
                            return message;

                        }else {

                            message = "failure";
                            return message;

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        message = "failure";
                        return message;
                    }

                }

                @Override
                protected void onPostExecute(String m) {
                    super.onPostExecute(m);

                    if(m.equals("success")){

                        try {

                            Log.v(TAG, "New trip updated in DB");
                            getCurrentTripStatusFromDB();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }else{

                        Log.v(TAG, "Error on updating to ongoing status trip in DB");

                    }

                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");


        }


        void getLastLocation(){

            if (checkPermission(MY_PERMISSIONS_REQUEST_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, "Location Access Permission needed, please grant")) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    latitudeValue = mLastLocation.getLatitude();
                    longitudeValue = mLastLocation.getLongitude();
                    Log.d(TAG, "Latitude 4: " + latitudeValue + " Longitude 4: " + longitudeValue);
                    refreshMap(mMap);
                    markStartingLocationOnMap(mMap, new LatLng(latitudeValue, longitudeValue));
                    startPolyline(mMap, new LatLng(latitudeValue, longitudeValue));
                }
            }

        }


        private List<LatLng> getPoints(List<LocationObject> mLocations){
            List<LatLng> points = new ArrayList<LatLng>();
            for(LocationObject mLocation : mLocations){
                points.add(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()));
            }
            return points;
        }
        private void startPolyline(GoogleMap map, LatLng location){
            if(map == null){
                Log.d(TAG, "Map object is not null");
                return;
            }
            PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
            options.add(location);
            Polyline polyline = map.addPolyline(options);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(location)
                    .zoom(16)
                    .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        private void drawRouteOnMap(GoogleMap map, List<LatLng> positions){
            PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
            options.addAll(positions);
            Polyline polyline = map.addPolyline(options);
            int current_location = positions.size() - 1;
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    //.target(new LatLng(positions.get(0).latitude, positions.get(0).longitude))
                    .target(new LatLng(positions.get(current_location).latitude, positions.get(current_location).longitude))
                    .zoom(17)
                    .bearing(90)
                    .tilt(40)
                    .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        private void refreshMap(GoogleMap mapInstance){
            mapInstance.clear();
        }
        public LocationRequest createLocationRequest() {
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(5000);
            mLocationRequest.setFastestInterval(3000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            return mLocationRequest;
        }

        public void captureMapsScreen()
        {
            GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback()
            {

                @Override
                public void onSnapshotReady(Bitmap snapshot)
                {
                    // TODO Auto-generated method stub
                    String file_name = System.currentTimeMillis() + ".jpeg";
                    String rootDir = Environment.getExternalStorageDirectory() + File.separator + "trippoints" + File.separator + "maps";
                    File rootFile = new File(new File(Environment.getExternalStorageDirectory(), "trippoints"), "maps");
                    rootFile.mkdirs();
                    String path = rootDir + "/" + file_name;

                    Bitmap bitmap = snapshot;



                    try
                    {
                        OutputStream fout = new FileOutputStream(path);

                        // Write the string to the file
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fout);
                        fout.flush();
                        fout.close();
                    }
                    catch (FileNotFoundException e)
                    {
                        // TODO Auto-generated catch block
                        Log.d("ImageCapture", "FileNotFoundException");
                        Log.d("ImageCapture", e.getMessage());
                        path = "";
                    }
                    catch (IOException e)
                    {
                        // TODO Auto-generated catch block
                        Log.d("ImageCapture", "IOException");
                        Log.d("ImageCapture", e.getMessage());
                        path = "";
                    }

                    goToSaveTripScreen(path);
                }
            };

            mMap.snapshot(callback);
        }


        public void goToSaveTripScreen(String map){

            Intent save_trip_screen = new Intent(getActivity(), Save_Trip_Screen.class);
            save_trip_screen.putExtra("map", map);
            startActivity(save_trip_screen);
            removeAllLocationsfromDB();

        }


        @Override
        public void onResume() {
            super.onResume();
            int trip_status = Preference.getInt(getActivity(), "current_trip_status");
            if(trip_status != TRIP_ONGOING){

                getLastLocation();

            }
        }
        @Override
        public void onPause() {
            super.onPause();
            //LocalBroadcastManager.getInstance(this).unregisterReceiver(routeReceiver);
        }
        @Override
        public void onStart() {
            mGoogleApiClient.connect();
            getActivity().getApplicationContext().registerReceiver(routeReceiver, new IntentFilter("proponomultimedia.citerio.com.trippoints"));
            super.onStart();
        }
        @Override
        public void onStop() {
            mGoogleApiClient.disconnect();
            super.onStop();
        }
        @Override
        public void onDestroy() {
            super.onDestroy();
            getActivity().getApplicationContext().unregisterReceiver(routeReceiver);
        }



        /*@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        public boolean checkPermission(int code)
        {
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if(currentAPIVersion >= android.os.Build.VERSION_CODES.M)
            {

                if(code == MY_PERMISSIONS_REQUEST_FINE_LOCATION){

                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                            alertBuilder.setCancelable(true);
                            alertBuilder.setTitle("Permission Necessary");
                            alertBuilder.setMessage("Location Access Permission needed, please grant");
                            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_FINE_LOCATION);
                                }
                            });
                            AlertDialog alert = alertBuilder.create();
                            alert.show();
                        } else {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_FINE_LOCATION);
                        }
                        return false;
                    } else {
                        return true;
                    }

                }else if(code == MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE){

                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                            alertBuilder.setCancelable(true);
                            alertBuilder.setTitle("Permission Necessary");
                            alertBuilder.setMessage("Write External Storage Permission needed, please grant");
                            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                public void onClick(DialogInterface dialog, int which) {
                                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                                }
                            });
                            AlertDialog alert = alertBuilder.create();
                            alert.show();
                        } else {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                        return false;
                    } else {
                        return true;
                    }
                }

            } else {
                return true;
            }
        }*/



        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        public boolean checkPermission(final int requestCode, final String manifestPermission, String permissionMessage)
        {
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if(currentAPIVersion >= android.os.Build.VERSION_CODES.M)
            {
                if (ContextCompat.checkSelfPermission(getActivity(), manifestPermission) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), manifestPermission)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Permission Necessary");
                        alertBuilder.setMessage(permissionMessage);
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(new String[]{manifestPermission}, requestCode);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                    } else {
                        requestPermissions(new String[]{manifestPermission}, requestCode);
                    }
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }


        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            //Log.v(LOG, "onRequestPersmission called");
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_FINE_LOCATION:
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        Snackbar
                                .make(getActivity().findViewById(android.R.id.content), "Permission Granted", Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                })
                                .show();

                        if(checkPermission(MY_PERMISSIONS_REQUEST_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, "Location Access Permission needed, please grant")){

                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                            if (mLastLocation != null) {
                                latitudeValue = mLastLocation.getLatitude();
                                longitudeValue = mLastLocation.getLongitude();
                                Log.d(TAG, "Latitude 4: " + latitudeValue + " Longitude 4: " + longitudeValue);
                                refreshMap(mMap);
                                markStartingLocationOnMap(mMap, new LatLng(latitudeValue, longitudeValue));
                                startPolyline(mMap, new LatLng(latitudeValue, longitudeValue));
                            }

                        }


                    } else {


                        Snackbar
                                .make(getActivity().findViewById(android.R.id.content), "Please Grant Permission", Snackbar.LENGTH_INDEFINITE)
                                .setAction("Try again", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        checkPermission(MY_PERMISSIONS_REQUEST_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, "Location Access Permission needed, please grant");
                                    }
                                })
                                .show();

                    }

                    break;
                case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        Snackbar
                                .make(getActivity().findViewById(android.R.id.content), "Permission Granted", Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        captureMapsScreen();
                                    }
                                })
                                .show();


                    } else {


                        Snackbar
                                .make(getActivity().findViewById(android.R.id.content), "Please Grant Permission", Snackbar.LENGTH_INDEFINITE)
                                .setAction("Try again", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        checkPermission(MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, "Write External Storage Permission needed, please grant");
                                    }
                                })
                                .show();

                    }

                    break;
            }
        }


    }

    /**
     * A Book fragment containing a simple view.
     */

    public static class BookFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private RecyclerView trip_list;
        private RecyclerView.LayoutManager trip_LayoutManager;
        private TextView no_trips_recorded;
        private final int TRIP_PENDING = 1;
        private final int TRIP_APPROVED = 2;
        private final int TRIP_ONGOING = 3;
        private final int TRIP_FINISHED = 4;


        public BookFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static BookFragment newInstance(int sectionNumber) {
            BookFragment fragment = new BookFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_book_tabs_screen, container, false);
            trip_list = (RecyclerView) rootView.findViewById(R.id.trip_list);
            no_trips_recorded = (TextView) rootView.findViewById(R.id.no_trips_recorded);

            trip_LayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            trip_list.setLayoutManager(trip_LayoutManager);
            trip_list.setItemAnimator(new DefaultItemAnimator());
            //trip_list.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

            showSectionBody();

            return rootView;
        }

        public void showSectionBody(){


            new AsyncTask<String, Void, TripAdapter>() {


                private TripAdapter trip_adapter = null;

                @Override
                protected TripAdapter doInBackground(String... params) {


                    /////////////////////////////////Video section/////////////////////////////////
                    AppDatabase database = AppDatabase.getDatabase(getActivity().getApplicationContext());
                    trip_adapter = new TripAdapter(getActivity(), (ArrayList<Trip>) database.tripDao().getTrips(getCurrentUserId(), TRIP_FINISHED));


                    return trip_adapter;
                }

                @Override
                protected void onPostExecute(TripAdapter s) {
                    super.onPostExecute(s);

                    if(trip_adapter != null){

                        trip_list.setAdapter(trip_adapter);

                        if(trip_list.getAdapter().getItemCount() == 0){

                            no_trips_recorded.setVisibility(View.VISIBLE);
                        }


                    }else{

                        Toast.makeText(getActivity().getApplicationContext(), "trip adapter null",  Toast.LENGTH_LONG).show();


                    }

                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");



        }

        int getCurrentUserId(){


            String current_user = Preference.getString(getActivity(), "current_user");


            if(!current_user.isEmpty()){

                try {

                    JSONObject jsonObject_current_user = new JSONObject(current_user);

                    return jsonObject_current_user.getInt("id");

                } catch (JSONException e) {


                    e.printStackTrace();
                }


            }

            return 0;



        }
    }

    /**
     * A Export fragment containing a simple view.
     */

    public static class ExportFragment extends Fragment implements DatePickerDialog.OnDateSetListener{
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final int START_DATE = 0;
        private static final int END_DATE = 1;
        private int date_type = 0;
        private TextView start_date, end_date;
        private static final String TAG = "MyActivity";
        public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 123;
        private final int TRIP_FINISHED = 4;
        private Date start_date_search, end_date_search;
        private Button export_button;
        private Spinner trip_type, trip_file_format;
        private final int TRIP_TYPE_BUSINESS = 0;
        private final int TRIP_TYPE_PRIVATE = 1;
        private final int TRIP_FILE_FORMAT_CSV = 0;
        private final int TRIP_FILE_FORMAT_PDF = 1;
        private SimpleDateFormat dateFormatTextView;
        private SimpleDateFormat dateFormatDataBase;


        public ExportFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ExportFragment newInstance(int sectionNumber) {
            ExportFragment fragment = new ExportFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_export_tabs_screen, container, false);
            start_date = (TextView) rootView.findViewById(R.id.start_date);
            end_date = (TextView) rootView.findViewById(R.id.end_date);
            export_button = (Button) rootView.findViewById(R.id.export_button);
            trip_type = (Spinner) rootView.findViewById(R.id.trip_type);
            trip_file_format = (Spinner) rootView.findViewById(R.id.trip_file_format);

            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> trip_type_adapter = ArrayAdapter.createFromResource(getActivity(),
                    R.array.type_trip_export, R.layout.brand_item);
            // Specify the layout to use when the list of choices appears
            trip_type_adapter.setDropDownViewResource(R.layout.spinner_item);
            // Apply the adapter to the spinner
            trip_type.setAdapter(trip_type_adapter);

            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> trip_format_adapter = ArrayAdapter.createFromResource(getActivity(),
                    R.array.file_format_export, R.layout.brand_item);
            // Specify the layout to use when the list of choices appears
            trip_format_adapter.setDropDownViewResource(R.layout.spinner_item);
            // Apply the adapter to the spinner
            trip_file_format.setAdapter(trip_format_adapter);

            dateFormatTextView = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);
            dateFormatDataBase = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

            Date current_date = new Date();

            start_date.setText(dateFormatTextView.format(current_date));
            end_date.setText(dateFormatTextView.format(current_date));
            try {
                start_date_search = dateFormatDataBase.parse(dateFormatDataBase.format(current_date));
                end_date_search = dateFormatDataBase.parse(dateFormatDataBase.format(current_date));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            start_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    date_type = START_DATE;
                    showDatePickerDialog(v);
                }
            });

            end_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    date_type = END_DATE;
                    showDatePickerDialog(v);
                }
            });

            export_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(!start_date.getText().toString().trim().isEmpty() && !end_date.getText().toString().trim().isEmpty()){

                        if(checkPermission()){

                            exportTrips();

                        }

                    }

                }
            });


            return rootView;
        }

        public void setDate(Calendar calendar) {

            //final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);


            try {

                //String short_format = dateFormatDataBase.format(calendar.getTime());
                //Log.v(TAG, "short format: " + dateFormatDataBase.parse(short_format).toString());


                switch (date_type){
                    case START_DATE:
                        start_date_search = calendar.getTime();
                        start_date.setText(dateFormatTextView.format(calendar.getTime()));
                        break;
                    case END_DATE:
                        end_date_search = calendar.getTime();
                        end_date.setText(dateFormatTextView.format(calendar.getTime()));
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        int getCurrentUserId(){


            String current_user = Preference.getString(getActivity(), "current_user");


            if(!current_user.isEmpty()){

                try {

                    JSONObject jsonObject_current_user = new JSONObject(current_user);

                    return jsonObject_current_user.getInt("id");

                } catch (JSONException e) {


                    e.printStackTrace();
                }


            }

            return 0;



        }

        public void showDatePickerDialog(View v) {
            DialogFragment newFragment = new DatePickerFragment();
            newFragment.setTargetFragment(ExportFragment.this, 0);
            newFragment.show(getFragmentManager(), "datePicker");
        }

        public static class DatePickerFragment extends DialogFragment
        {
            private DatePickerDialog.OnDateSetListener dateSetListener;

            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                // Use the current date as the default date in the picker
                dateSetListener = (DatePickerDialog.OnDateSetListener)getTargetFragment();
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                // Create a new instance of DatePickerDialog and return it
                return new DatePickerDialog(getActivity(), dateSetListener, year, month, day);

            }

        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            Calendar cal = new GregorianCalendar(year, month, day);

            setDate(cal);

        }

        void exportTrips(){


            new AsyncTask<String, Void, String>(){

                private String message = "";
                private int type = 0;
                private int format = 0;
                private boolean any_trips = false;


                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    String trip_type_string = trip_type.getSelectedItem().toString();
                    if(trip_type_string.equals("Poslovanje")){

                        type = TRIP_TYPE_BUSINESS;


                    }else if(trip_type_string.equals("Privatna")){

                        type = TRIP_TYPE_PRIVATE;

                    }

                    String trip_file_format_string = trip_file_format.getSelectedItem().toString();
                    if(trip_file_format_string.equals("CSV")){

                        format = TRIP_FILE_FORMAT_CSV;


                    }else if(trip_file_format_string.equals("PDF")){

                        format = TRIP_FILE_FORMAT_PDF;

                    }
                }

                @Override
                protected String doInBackground(String... params) {

                    try {

                        AppDatabase database = AppDatabase.getDatabase(getActivity().getApplicationContext());
                        List<Trip> trips_to_export = new ArrayList<Trip>();
                        trips_to_export.addAll(database.tripDao().findTripsBetweenDates(getCurrentUserId(), TRIP_FINISHED, start_date_search, end_date_search, type));

                        if(trips_to_export.size() > 0){

                            any_trips = true;
                            String result = "";

                            if(format == TRIP_FILE_FORMAT_CSV){

                                result = createCSVfile(trips_to_export);

                            }else if(format == TRIP_FILE_FORMAT_PDF){

                                result = createPDFfile(trips_to_export);

                            }

                            if(!result.isEmpty()){

                                publishResults(result);

                            }else {

                                message = "failure";
                                return message;

                            }

                            message = "success";
                            return message;

                        }else {

                            message = "success";
                            return message;

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        message = "failure";
                        return message;
                    }

                }

                @Override
                protected void onPostExecute(String m) {
                    super.onPostExecute(m);

                    if(m.equals("success")){

                        try {

                            Log.v(TAG, "Working nice");

                            if(!any_trips){

                                Toast.makeText(getActivity().getApplicationContext(), "No trips match",  Toast.LENGTH_LONG).show();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }else{

                        Log.v(TAG, "Error on getting the CSV file");

                    }

                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");

        }

        public String createCSVfile(List<Trip> trips) {

            String path = "";

            try {
                String file_name = System.currentTimeMillis() + ".csv";
                String rootDir = Environment.getExternalStorageDirectory() + File.separator + "trippoints" + File.separator + "reports";
                File rootFile = new File(new File(Environment.getExternalStorageDirectory(), "trippoints"), "reports");
                rootFile.mkdirs();
                path = rootDir + "/" + file_name;

                CSVWriter writer = null;
                int trip_type_int = 0;
                String trip_type_string = "";

                writer = new CSVWriter(new FileWriter(path));

                writer.writeNext(new String[]{"Trip Points"});
                writer.writeNext(new String[]{""});
                writer.writeNext(new String[]{""});
                writer.writeNext(new String[]{"Date", "Start Time", "End Time", "From", "To", "Odometer Start", "Odometer End", "Distance", "Trip type", "Purpose", "Notes"});

                for(int i = 0; i < trips.size(); i++){

                    trip_type_int = trips.get(i).getType();

                    if(trip_type_int == TRIP_TYPE_BUSINESS){

                        trip_type_string = "Poslovanje";


                    }else if(trip_type_int == TRIP_TYPE_PRIVATE){

                        trip_type_string = "Privatna";

                    }

                    writer.writeNext(new String[]{new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(trips.get(i).getDate_start()), trips.get(i).getTime_start(), trips.get(i).getTime_end(), trips.get(i).getFrom(), trips.get(i).getTo(), Integer.toString(trips.get(i).getOdometer_start()), Integer.toString(trips.get(i).getOdometer_end()), Integer.toString(trips.get(i).getKm()), trip_type_string, trips.get(i).getPurpose(), trips.get(i).getNotes()});

                }

                writer.close();

                return path;

            } catch (IOException e) {
                Log.d("Error....", e.toString());
                e.printStackTrace();
                path = "";
            }

            return path;

        }

        public String createPDFfile(List<Trip> trips) {

            String path = "";

            try {
                String file_name = System.currentTimeMillis() + ".pdf";
                String rootDir = Environment.getExternalStorageDirectory() + File.separator + "trippoints" + File.separator + "reports";
                File rootFile = new File(new File(Environment.getExternalStorageDirectory(), "trippoints"), "reports");
                rootFile.mkdirs();
                path = rootDir + "/" + file_name;

                Document doc = new Document(PageSize.A4.rotate(), 2, 2, 2, 2);

                File file = new File(rootFile, file_name);
                FileOutputStream fOut = new FileOutputStream(file);

                PdfWriter.getInstance(doc, fOut);

                //open the document
                doc.open();

                int trip_type_int = 0;
                String trip_type_string = "";

                Paragraph p1 = new Paragraph("Trip Points");
                Font paraFont= new Font(Font.FontFamily.COURIER);
                p1.setAlignment(Paragraph.ALIGN_CENTER);
                p1.setFont(paraFont);

                //add paragraph to document
                doc.add(p1);
                doc.add( Chunk.NEWLINE );
                doc.add( Chunk.NEWLINE );


                PdfPTable table = new PdfPTable(11);
                table.setWidthPercentage(100f);

                table.addCell("Date");
                table.addCell("Start Time");
                table.addCell("End Time");
                table.addCell("From");
                table.addCell("To");
                table.addCell("Odometer Start");
                table.addCell("Odometer End");
                table.addCell("Distance");
                table.addCell("Trip type");
                table.addCell("Purpose");
                table.addCell("Notes");

                for(int i = 0; i < trips.size(); i++){

                    trip_type_int = trips.get(i).getType();

                    if(trip_type_int == TRIP_TYPE_BUSINESS){

                        trip_type_string = "Poslovanje";


                    }else if(trip_type_int == TRIP_TYPE_PRIVATE){

                        trip_type_string = "Privatna";

                    }

                    table.addCell(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(trips.get(i).getDate_start()));
                    table.addCell(trips.get(i).getTime_start());
                    table.addCell(trips.get(i).getTime_end());
                    table.addCell(trips.get(i).getFrom());
                    table.addCell(trips.get(i).getTo());
                    table.addCell(Integer.toString(trips.get(i).getOdometer_start()));
                    table.addCell(Integer.toString(trips.get(i).getOdometer_end()));
                    table.addCell(Integer.toString(trips.get(i).getKm()));
                    table.addCell(trip_type_string);
                    table.addCell(trips.get(i).getPurpose());
                    table.addCell(trips.get(i).getNotes());

                }

                doc.add(table);

                doc.close();

                return path;

            } catch (IOException e) {
                Log.d("Error....", e.toString());
                e.printStackTrace();
                path = "";
            } catch (DocumentException e) {
                e.printStackTrace();
                path = "";
            }

            return path;

        }

        public void publishResults(String file_path){

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Trip Points report");
            File file = new File(file_path);
            //Uri uri = Uri.fromFile(file);
            Uri uri = FileProvider.getUriForFile(getActivity(), "proponomultimedia.citerio.com.trippoints.provider", file);
            sendIntent.putExtra(Intent.EXTRA_STREAM, uri);

            Intent chooser = Intent.createChooser(sendIntent, "Dijeliti");

            // Verify the original intent will resolve to at least one activity
            if (sendIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(chooser);
            }else {
                Toast.makeText(getActivity().getApplicationContext(), "Error on sharing the doc",  Toast.LENGTH_LONG).show();
            }


        }


        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        public boolean checkPermission()
        {
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if(currentAPIVersion >= android.os.Build.VERSION_CODES.M)
            {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Permission Necessary");
                        alertBuilder.setMessage("Write External Storage Permission needed, please grant");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                    } else {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                    }
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }


        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            //Log.v(LOG, "onRequestPersmission called");
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        Snackbar
                                .make(getActivity().findViewById(android.R.id.content), "Permission Granted", Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        exportTrips();
                                    }
                                })
                                .show();


                    } else {


                        Snackbar
                                .make(getActivity().findViewById(android.R.id.content), "Please Grant Permission", Snackbar.LENGTH_INDEFINITE)
                                .setAction("Try again", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        checkPermission();
                                    }
                                })
                                .show();

                    }

                    break;
            }
        }




    }

    /**
     * A Settings fragment containing a simple view.
     */

    public static class SettingsFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final int REQUEST_EXIT = 567;
        private TextView settings_user_button, settings_server_button, settings_car_button, settings_about_button;

        public SettingsFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static SettingsFragment newInstance(int sectionNumber) {
            SettingsFragment fragment = new SettingsFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_settings_tabs_screen, container, false);
            settings_user_button = (TextView)rootView.findViewById(R.id.settings_user_button);
            settings_server_button = (TextView)rootView.findViewById(R.id.settings_server_button);
            settings_car_button = (TextView)rootView.findViewById(R.id.settings_car_button);
            settings_about_button = (TextView)rootView.findViewById(R.id.settings_about_button);

            settings_user_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent settings_user_screen = new Intent(getActivity(), Settings_User_Screen.class);
                    startActivity(settings_user_screen);
                }
            });

            settings_server_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent settings_server_screen = new Intent(getActivity(), Settings_Server_Screen.class);
                    startActivityForResult(settings_server_screen, REQUEST_EXIT);
                }
            });

            settings_car_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent settings_car_screen = new Intent(getActivity(), Settings_Car_Screen.class);
                    startActivity(settings_car_screen);
                }
            });

            settings_about_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent settings_about_screen = new Intent(getActivity(), Settings_About_Screen.class);
                    startActivity(settings_about_screen);
                }
            });
            return rootView;
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {

            if (requestCode == REQUEST_EXIT) {
                if (resultCode == RESULT_OK) {
                    getActivity().finish();

                }
            }
        }

    }

    /**
     * A About fragment containing a simple view.
     */

    public static class AboutFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public AboutFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static AboutFragment newInstance(int sectionNumber) {
            AboutFragment fragment = new AboutFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_about_tabs_screen, container, false);

            return rootView;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        decorView.setSystemUiVisibility(uiOptions);

    }


}
