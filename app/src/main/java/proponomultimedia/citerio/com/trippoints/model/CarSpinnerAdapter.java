package proponomultimedia.citerio.com.trippoints.model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import proponomultimedia.citerio.com.trippoints.R;

/**
 * Created by Jose Ricardo on 24/01/2017.
 */
public class CarSpinnerAdapter extends ArrayAdapter<Car> {

    private Context nContext;
    private ArrayList<Car> cars;

    public CarSpinnerAdapter(Context context, ArrayList<Car> cars) {
        super(context, 0, cars);
        this.nContext = context;
        this.cars = cars;
    }

    @Override
    public int getCount() {
        return cars.size();
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Car item = getItem(position);

        if(convertView == null){

            convertView = LayoutInflater.from(nContext).inflate(R.layout.car_spinner, parent, false);

        }


        TextView car_id = (TextView) convertView.findViewById(R.id.id);
        TextView car_name = (TextView) convertView.findViewById(R.id.name);
        TextView car_plate = (TextView) convertView.findViewById(R.id.plate);
        TextView car_manufaturer = (TextView) convertView.findViewById(R.id.manufacturer);


        car_id.setText(Integer.toString(item.getId()));
        car_name.setText(item.getName());
        car_plate.setText(item.getPlate());
        car_manufaturer.setText(item.getManufacturer());

        if (item instanceof CharSequence) {
            car_name.setText((CharSequence)item);
        }
        else {
            car_name.setText(item.toString());
        }


        return convertView;
    }

}
