package proponomultimedia.citerio.com.trippoints.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

/**
 * Created by Protinal on 08/01/2018.
 */

@Entity
public class Trip {

    @PrimaryKey
    private int id;
    private Date date_start;
    private Date date_end;
    private String time_start;
    private String time_end;
    private String purpose;
    private String from;
    private String to;
    private int odometer_start;
    private int odometer_end;
    private int type;
    private int km;
    private int amount;
    private String notes;
    private int user;
    private int status;
    private String map;
    private int car;


    public Trip(int id, Date date_start, Date date_end, String time_start, String time_end, String purpose, String from, String to, int odometer_start, int odometer_end, int type, int km, int amount, String notes, int user, int status, String map, int car) {
        this.id = id;
        this.date_start = date_start;
        this.date_end = date_end;
        this.time_start = time_start;
        this.time_end = time_end;
        this.purpose = purpose;
        this.from = from;
        this.to = to;
        this.odometer_start = odometer_start;
        this.odometer_end = odometer_end;
        this.type = type;
        this.km = km;
        this.amount = amount;
        this.notes = notes;
        this.user = user;
        this.status = status;
        this.map = map;
        this.car = car;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate_start() {
        return date_start;
    }

    public void setDate_start(Date date_start) {
        this.date_start = date_start;
    }

    public Date getDate_end() {
        return date_end;
    }

    public void setDate_end(Date date_end) {
        this.date_end = date_end;
    }

    public String getTime_start() {
        return time_start;
    }

    public void setTime_start(String time_start) {
        this.time_start = time_start;
    }

    public String getTime_end() {
        return time_end;
    }

    public void setTime_end(String time_end) {
        this.time_end = time_end;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getOdometer_start() {
        return odometer_start;
    }

    public void setOdometer_start(int odometer_start) {
        this.odometer_start = odometer_start;
    }

    public int getOdometer_end() {
        return odometer_end;
    }

    public void setOdometer_end(int odometer_end) {
        this.odometer_end = odometer_end;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public int getCar() {
        return car;
    }

    public void setCar(int car) {
        this.car = car;
    }
}
