package proponomultimedia.citerio.com.trippoints.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Protinal on 26/01/2018.
 */

@Entity
public class User {

    @PrimaryKey
    private int id;
    private String name;
    private String company;
    private String address;
    private String phone_number;
    private String email;
    private String password;
    private String token;

    public User(int id, String name, String company, String address, String phone_number, String email, String password, String token) {
        this.id = id;
        this.name = name;
        this.company = company;
        this.address = address;
        this.phone_number = phone_number;
        this.email = email;
        this.password = password;
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
